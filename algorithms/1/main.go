package main

import "fmt"

// What is the following code output, why?

func main() {
	//make([]T, length, capacity)
	s1 := []int{1, 2, 3}
	fmt.Println(s1, "Haha") // [1 2 3] Haha

	s2 := s1
	fmt.Println(s1, "Haha") // [1 2 3] Haha
	for i := 0; i < 3; i++ {
		s2[i] = s2[i] + 1 // [2 3 4]
	}
	fmt.Println(s1) // [2 3 4]
	fmt.Println(s2) // [2 3 4]
}
