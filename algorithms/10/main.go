package main

import (
	"fmt"
)

// What is the output below?
func main() {
	defer_call()
}
func defer_call() {
	defer func() { fmt.Println("Before printing") }()
	defer func() { fmt.Println("Print") }()
	defer func() { fmt.Println("Print") }()
	panic("Trigger anomalies")
}

// Print
// Print
// Before printing
// Trigger anomalies
