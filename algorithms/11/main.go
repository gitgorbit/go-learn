package main

import (
	"fmt"
)

// Traverse each element of the slice, an element access is performed by a given function.
func visit(list []int, f func(int)) {
	for _, v := range list {
		f(v)
	}
}
func main() {
	// Print the sliced ​​content using anonymous function
	visit([]int{1, 2, 3, 4}, func(v int) {
		fmt.Println(v)
	})
}
