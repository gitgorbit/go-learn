package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	// GomaxProcs Set the maximum number of CPUs that can be executed simultaneously and return to the previous settings.
	// If n <1, it will not change the current settings. The number of logical CPUs for local machines can be queried by Numcpu.
	// This function will be removed after the scheduler is optimized.
	runtime.GOMAXPROCS(1)
	wg := sync.WaitGroup{}
	wg.Add(20)
	for i := 0; i < 10; i++ { // Main thread
		go func() { // Threaded thread
			fmt.Println("A: ", i)
			wg.Done()
		}()
	}
	for i := 0; i < 10; i++ {
		go func(i int) {
			fmt.Println("B: ", i)
			wg.Done()
		}(i)
	}
	wg.Wait()
}
