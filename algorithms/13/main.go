package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	// GomaxProcs Set the maximum number of CPUs that can be executed simultaneously and return to the previous settings.
	// If n <1, it will not change the current settings. The number of logical CPUs for local machines can be queried by Numcpu.
	// This function will be removed after the scheduler is optimized.
	runtime.GOMAXPROCS(2)
	wg := sync.WaitGroup{}
	wg.Add(3)
	for i := 0; i < 10; i++ { // Main thread
		go func() { // Threaded thread
			fmt.Println("A: ", i)
			wg.Done()
		}()

	}
	wg.Wait()
	wg.Add(7)
	wgg := sync.WaitGroup{}
	wgg.Add(5)
	for i := 0; i < 10; i++ {
		go func(i int) {
			fmt.Println("B: ", i)
			wgg.Done()

		}(i)
	}
	wgg.Wait()

}

// not ordered, not synchronized.
