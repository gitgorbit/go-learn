package main

import (
	"fmt"
	"sync"
)

// What is the problem with the code below, how to modify
type UserAges struct {
	ages map[string]int
	sync.Mutex
}

func (ua *UserAges) Add(name string, age int) {
	//ua.ages = make(map[string]int) Fix for the issue
	ua.Lock()
	defer ua.Unlock()
	ua.ages[name] = age
}
func (ua *UserAges) Get(name string) int {
	if age, ok := ua.ages[name]; ok {
		return age
	}
	return -1
}

func main() {
	var userAges UserAges
	userAges.Add("lisa", 24)
	userAges.Get("lisa")
	fmt.Println(userAges.ages)
}

// ages prop is not initialized
