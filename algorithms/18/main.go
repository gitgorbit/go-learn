package main

// What is the following code output? why?
func main() {
	i := GetValue()
	switch i.(type) {
	case int:
		println("int")
	case string:
		println("string")
	case interface{}:
		println("interface")
	default:
		println("unknown")
	}
}

// Fix: Return interface since i.(type) can run only against interface
// func GetValue() int {
func GetValue() interface{} {
	return 1
}
