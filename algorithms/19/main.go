package main

import "fmt"

func main() {
	s1 := []int{1, 2, 3}
	s2 := []int{4, 5}
	s1 = append(s1, s2...)
	fmt.Println(s1)
	fmt.Println(s2)
	var a = []int{1, 2, 3}
	a = append([]int{0}, a...)
	fmt.Println(a)
	a = append([]int{-3, -2, -1}, a...)
	fmt.Println(a)

}

// 1 2 3 4 5
// 4 5
// 0 1 2 3
// -3 -2 -1 0 1 2 3
