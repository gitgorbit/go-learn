package main

import "fmt"

func rmLast(a []int) {
	fmt.Println("a", a) // a [1 2 3 4 5 6 7 8 9]
	a = a[:len(a)-1]    // [1 2 3 4 5 6 7 8]
	for i := 0; i < 13; i++ {
		a = append(a, 8) // [1 2 3 4 5 6 7 8 8 8 8 8 8 8 8 8 8 8 8 8 8]
	}

	fmt.Println("RM after A", a) // RM after A [1 2 3 4 5 6 7 8 8 8 8 8 8 8 8 8 8 8 8 8 8]
	// fmt.Println(len(a))
}
func updateLast(a []int) {
	fmt.Println("a", a) // a [1 2 3 4 5 6 7 8]
	for i := 0; i < len(a); i++ {
		a[i] = a[i] + 1
	}
	fmt.Println("Modified A", a) // [2 3 4 5 6 7 8 9 9 9 9 9 9 9 9 9 9 9 9 9 9]
	// fmt.Println(len(a))
}
func main() {
	xyz := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println("xyz", xyz) // xyz [1 2 3 4 5 6 7 8 9]
	rmLast(xyz)
	fmt.Println("r   xyz", xyz) // r   xyz [1 2 3 4 5 6 7 8 8] - didn't understand ???
	updateLast(xyz)
	fmt.Println("Modified XYZ", xyz)
}
