package main

import (
	"fmt"
)

func main() {
	// It can be buffered or unbuffered
	// c := make(chan int, 2)
	c := make(chan int, 2)
	go func() {
		var xValue int
		for x := 0; x <= 3; x++ {
			xValue += x
		}
		c <- xValue

	}()
	go func() {
		var iValue int
		for i := 0; i <= 2; i++ {
			iValue += i
		}
		c <- iValue
	}()

	fmt.Println("len", len(c), "cap", cap(c))
	sum := 0
	for i := 0; i < cap(c); i++ {
		sum += <-c
	}

	fmt.Println("sumValue", sum)
	// x, y := <-c, <-c
	// fmt.Println("sumValue", x+y)

}
