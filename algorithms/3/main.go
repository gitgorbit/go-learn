package main

import "fmt"

//  What is the output below? Why?
func main() {
	// n1 is the underlying array of N2
	n1 := [3]int{1, 2, 3}
	n2 := n1[0:3]
	fmt.Println("Below is the N1 address")
	for i := 0; i < len(n1); i++ {
		fmt.Printf("%p\n", &n1[i])

	}
	fmt.Println(n1)

	fmt.Println("Below is N2 address")
	for i := 0; i < len(n2); i++ {
		fmt.Printf("%p\n", &n2[i])

	}
	fmt.Println(n2)

	n2 = append(n2, 1)
	fmt.Println("Below is the N1 address")
	for i := 0; i < len(n1); i++ {
		fmt.Printf("%p\n", &n1[i])

	}
	fmt.Println(n1)

	fmt.Println("Below is N2 address")
	for i := 0; i < len(n2); i++ {
		fmt.Printf("%p\n", &n2[i])
	}
	fmt.Println(n2)

}
