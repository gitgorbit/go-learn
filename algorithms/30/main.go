package main

func main() {
	done := make(chan struct{}) // Used to end the event
	c := make(chan int)         // Data Transport Channel
	go func() {
		// same as close(done)
		// defer func() {
		// 	done <- struct{}{}
		// }()
		defer close(done)  // Notification Close
		for x := range c { // Cycle read the message until the channel is turned off
			println(x)
		}
	}()
	c <- 1
	c <- 2
	c <- 3
	close(c) // Channel Close Remove Block
	<-done   // Block until there is data or notification to close

}
