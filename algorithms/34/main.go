package main

import "sync"

func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	c := make(chan int)
	var send chan<- int = c // only send
	var recv <-chan int = c // Receive only
	go func() {             //Receiving end
		defer wg.Done()
		for x := range recv {
			println(x)
		}
	}()
	go func() { // Transmit
		defer wg.Done()
		defer close(c)
		for i := 0; i < 3; i++ {
			send <- i
		}
	}()
	wg.Wait()
}
