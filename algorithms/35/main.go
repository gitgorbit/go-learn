package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	a, b := make(chan []int), make(chan []int)
	go func() { //Receiving end
		defer wg.Done()
		for {
			var (
				name string
				x    []int
				ok   bool
			)
			select { // Random selection available available Channel receive data
			case x, ok = <-a:
				name = "a"
			case x, ok = <-b:
				name = "b"
			}
			if !ok {
				return // If either channel is turned off, terminate the reception
			}
			x[0] = 99
			fmt.Println(name, x) // Output received data information
		}
	}()

	i := []int{1, 2, 3, 4}
	go func() { // Transmit
		defer wg.Done()
		defer close(a)
		defer close(b)
		fmt.Println(i)
		select { // Random selection of Channel
		case a <- i:
		}

	}()
	wg.Wait()
	fmt.Println(i)
}
