package main

import "fmt"

// What is the output below? Why?
func defer_call(y int) {
	for i := 0; i < 5; i++ {
		defer fmt.Println("Output Y + I", y+i)
		fmt.Println("Haha")
		defer fmt.Println("Output I", i)
	}

}
func main() {
	defer_call(5)
}

// Haha
// Haha
// Haha
// Haha
// Haha
// Output I 4
// Output Y + I 9
// Output I 3
// Output Y + I 8
// Output I 2
// Output Y + I 7
// Output I 1
// Output Y + I 6
// Output I 0
// Output Y + I 5
