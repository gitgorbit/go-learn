package main

import (
	"fmt"
	"time"
)

// What is the following code output?
func main() {
	for i := 0; i < 5; i++ {
		fmt.Println(i, "haha")
		// Anonymous function: Anonymous function can be called after declaration
		go func() {
			time.Sleep(3 * time.Second)

			fmt.Println(i, "Well")
		}()
	}

	time.Sleep(10 * time.Second)

}

// 0 haha
// 1 haha
// 2 haha
// 3 haha
// 4 haha
// 5 Well
// 5 Well
// 5 Well
// 5 Well
// 5 Well
