package main

import (
	"fmt"
	"time"
)

// What is the output below?
func main() {
	strs := []string{"one", "two", "three"}
	for _, s := range strs {
		// 1. The main thread is over (the main thread runs to Three), and the thread that is sent is just starting.
		// 2. Here the anonymous function is not called, it is the address of the shared S, so the print comes out is Three.
		go func() {

			time.Sleep(1 * time.Second)

			fmt.Printf("%s ", s)
		}()

		// Fix: add parameter:
		// go func(s string) {

		// 	time.Sleep(1 * time.Second)

		// 	fmt.Printf("%s ", s)
		// }(s)
	}
	time.Sleep(3 * time.Second)
}

// last value will be printed
// three three three
