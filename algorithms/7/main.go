package main

import (
	"fmt"
	"time"
)

// What is the following code output?
func main() {
	strs := []string{"one", "two", "three"}
	resultCh := make(chan string)

	for _, s := range strs {
		go func(s string) {
			resultCh <- s
			time.Sleep(1 * time.Second)

			fmt.Printf("%s ", s)
		}(s)
	}
	for i := 0; i < len(strs); i++ {
		r := <-resultCh
		fmt.Println(r)
	}

	// for r := range resultCh {
	// 	fmt.Println(r)
	// }
	// time.Sleep(3 * time.Second)
}

// order is not synchronized, so every time it is in different order
// because of a time reading all values succeeded
