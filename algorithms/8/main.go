package main

import "fmt"

// What is the output below?
func main() {

	x := []string{"ha", "b", "c"}

	for v := range x {

		fmt.Print(v)

	}

}

//012
