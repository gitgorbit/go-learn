package main

import "fmt"

type Client struct {
	name string
}

func main() {
	c := Client{name: "amel"}

	clients := []Client{}

	clients = append(clients, c)
	fmt.Println("len before adding a list: ", len(clients))

	//another list
	c2 := Client{name: "amel"}

	clients2 := []Client{}
	clients2 = append(clients2, c2)

	//append list to list
	fmt.Println(len(clients2) > 0)

	clients = append(clients, clients2...)
	fmt.Println("len after adding a list: ", len(clients))
}
