package main

import (
	"fmt"
)

func main() {
	array1 := []int{1, 3, 4, 5}
	array2 := []int{2, 4, 6, 8}

	last := len(array1) - 1
	array1 = append(array1, array1[last]) // Step 1
	copy(array1[2:], array1[1:last])      // Step 2
	array1[1] = array2[2]                 // Step 3

	fmt.Println(array1)
}
