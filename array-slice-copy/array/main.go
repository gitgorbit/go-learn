package main

import "fmt"

// https://golangbyexample.com/copy-an-array-or-slice-golang/

// A copy of the array will be automatically created when:
// 1. An array variable is assigned to another array variable.
// 2. An array variable is passed as an argument to a function.

func main() {
	sample1 := [2]string{"a", "b"}
	fmt.Printf("Sample1 Before: %v\n", sample1)
	sample2 := sample1
	sample2[1] = "c"
	fmt.Printf("Sample1 After assignment: %v\n", sample1)
	fmt.Printf("Sample2: %v\n", sample2)
	test(sample1)
	fmt.Printf("Sample1 After Test Function Call: %v\n", sample1)
}

func test(sample [2]string) {
	sample[0] = "d"
	fmt.Printf("Sample in Test function: %v\n", sample)
}

// Output:
// Sample1 Before [a b]
// Sample1 After assignment: [a b]
// Sample2 [a c]
// Sample in Test function: [d b]
// Sample1 After Test Function Call: [a b]
