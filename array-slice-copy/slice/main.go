package main

import "fmt"

// https://golangbyexample.com/copy-an-array-or-slice-golang/

// Since slice is a reference type, changing a copy will affect original data(sample1)

func main() {
	sample1 := []string{"a", "b"}
	fmt.Printf("Sample1 Before: %v\n", sample1)
	sample2 := sample1
	sample2[1] = "c"
	fmt.Printf("Sample1 After assignment: %v\n", sample1)
	fmt.Printf("Sample2: %v\n", sample2)
	test(sample1)
	fmt.Printf("Sample1 After Test Function Call: %v\n", sample1)
}

func test(sample []string) {
	sample[0] = "d"
	fmt.Printf("Sample in Test function: %v\n", sample)
}

// Output:
// Sample1 Before: [a b]
// Sample1 After assignment: [a c]
// Sample2: [a c]
// Sample in Test function: [d c]
// Sample1 After Test Function Call: [d c]
