package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

type Configuration struct {
	name string
}

func (c *Configuration) Print(surname string) {
	fmt.Print("printed: ", surname)
}

type Client struct {
	configurations map[string]*Configuration
}

func (c *Client) Init() {
	c.configurations = make(map[string]*Configuration)
	files := []string{"file1", "file2", "file3"}
	for index, f := range files {
		c.configurations[f] = &Configuration{name: strconv.Itoa(index)}
	}
}

func (c *Client) Get(name string) (*Configuration, error) {
	instance, ok := c.configurations[name]
	if ok {
		return instance, nil
	}
	return nil, fmt.Errorf("not configured")
}

func (c *Client) Print(name, surname string) {
	conf, err := c.Get(name)
	if err != nil {
		fmt.Println(err)
	}
	conf.Print(surname)
}

func main() {
	client := &Client{}
	client.Init()

	cl1, err := client.Get("file3")
	if err != nil {
		log.Fatal(err)
		// fmt.Println(err)
	}
	fmt.Println(cl1.name)
	// cl1.Print("husic")

	files, err := ioutil.ReadDir("/tmp/haproxy-spoe")
	if err != nil {
		fmt.Println(err)
	}
	for _, f := range files {
		i, err := os.Stat("/tmp/haproxy-spoe/" + f.Name())
		if err != nil {
			fmt.Println(err)
		}
		if i.IsDir() == false {
			fmt.Println(f.Name())
		}
	}

	// client.Print("file3", "husic")
}
