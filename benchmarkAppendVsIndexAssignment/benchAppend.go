package main

import "fmt"

func main() {

}

func arrayIndex() {
	data := data()
	result := make([]string, len(data))
	index := 0
	for key := range data {
		result[index] = key
		index++
	}
}

func appendOnly() {
	data := data()
	result := make([]string, 0, len(data))

	for key := range data {
		result = append(result, key)
	}
}

func appendNoLength() {
	data := data()
	result := make([]string, len(data))

	for key := range data {
		result = append(result, key)
	}
}

func data() map[string]string {
	data := make(map[string]string)

	for i := 0; i < 100000; i++ {
		v := fmt.Sprintf("a-%v", i)
		data[v] = v
	}
	return data
}
