package main

import (
	"testing"
)

func BenchmarkArrayIndex(b *testing.B) {
	for n := 0; n < b.N; n++ {
		arrayIndex()
	}
}

func BenchmarkAppendOnly(b *testing.B) {
	for n := 0; n < b.N; n++ {
		appendOnly()
	}
}

func BenchmarkAppendNotAllocated(b *testing.B) {
	for n := 0; n < b.N; n++ {
		appendNoLength()
	}
}
