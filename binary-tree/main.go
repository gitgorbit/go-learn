package main

import (
	"fmt"

	"golang.org/x/tour/tree"
)

func main() {
	same := Same(tree.New(1), tree.New(1))
	fmt.Println(same)

	t := tree.New(1)
	values := make(chan int)
	go Walk(t, values)
	for {
		v, ok := <-values
		if ok {
			fmt.Println(v)
		} else {
			return
		}
	}
}

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
	// <- closes the channel when this function returns
	defer close(ch)

	var walk func(t *tree.Tree)
	walk = func(t *tree.Tree) {
		if t == nil {
			return
		}
		walk(t.Left)
		ch <- t.Value
		walk(t.Right)

	}
	walk(t)
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {
	ch1, ch2 := make(chan int), make(chan int)

	go Walk(t1, ch1)
	go Walk(t2, ch2)

	for {
		v1, ok1 := <-ch1
		v2, ok2 := <-ch2

		if v1 != v2 || ok1 != ok2 {
			return false
		}

		if !ok1 {
			break
		}
	}

	return true
}
