package main

import "fmt"

func multipleReturn(x int, s string) (int, string) {
	return x, s
}

func main() {
	_, name := multipleReturn(10, "amel") // _ is only for purpose of ommiting first return value from function
	fmt.Println(name)
}
