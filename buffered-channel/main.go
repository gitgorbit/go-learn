package main

import (
	"fmt"
)

// https://www.geeksforgeeks.org/buffered-channel-in-golang/
// Generally buffered channels are usefull when you want to block a routine until a read occurs
func main() {

	// chan cap is 2, but in write func 4 values are sent.
	// This is a blocking call
	c := make(chan int, 1)
	go write(c)
	// add sleep to simulate that third and fourth values are written after a read completed
	//time.Sleep(2 * time.Second)

	for v := range c {
		fmt.Println("read ", v)
	}

	// for {
	// 	v, ok := <-c
	// 	if ok {
	// 		fmt.Println(v)
	// 	} else {
	// 		return
	// 	}

	// }

	fmt.Println("done")
}

func write(c chan<- int) {
	for i := 0; i < 4; i++ {
		fmt.Println("write", i)
		c <- i
	}
	// signal that job is over
	close(c)
}
