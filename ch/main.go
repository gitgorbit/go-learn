package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	result := StrChallenge("twominusoneplusfour") // 2 - 1 + 4 = 5
	// result := StrChallenge("threeplusfiveminusoneplusfour") // 3 + 5 - 1 + 4 = 11
	fmt.Println(result)
}

func StrChallenge(str string) string {

	strNums := map[string]int{
		"zero":  0,
		"one":   1,
		"two":   2,
		"three": 3,
		"four":  4,
		"five":  5,
		"six":   6,
		"seven": 7,
		"eight": 8,
		"nine":  9,
	}
	// twominusoneplusfour
	str = strings.ReplaceAll(str, "minus", ",-,") // two,-,oneplusfour
	str = strings.ReplaceAll(str, "plus", ",+,")  // two,-,one,+,four
	all := strings.Split(str, ",")

	var result int
	for i := 0; i < len(all); i++ {
		if i == 0 {
			num := getValue(strNums, all[i])
			result = num
		}
		if all[i] == "+" && i < len(all)-1 {
			num := getValue(strNums, all[i+1])
			result += num
			i++
		}
		if all[i] == "-" && i < len(all)-1 {
			num := getValue(strNums, all[i+1])
			result -= num
			i++
		}
	}
	numStr := map[int]string{
		0: "zero",
		1: "one",
		2: "two",
		3: "three",
		4: "four",
		5: "five",
		6: "six",
		7: "seven",
		8: "eight",
		9: "nine",
	}
	strNumber := strconv.Itoa(result)
	var strResult string
	for _, letter := range strNumber {
		n := int(letter - '0')
		key := mapkey(numStr, n)
		strResult += key
	}

	return strResult
}

func StrChallengeWithSplit(str string) string {

	strNums := map[string]int{
		"zero":  0,
		"one":   1,
		"two":   2,
		"three": 3,
		"four":  4,
		"five":  5,
		"six":   6,
		"seven": 7,
		"eight": 8,
		"nine":  9,
	}

	var allMinus []string

	minusSplit := strings.Split(str, "minus")

	for i := 0; i < len(minusSplit); i++ {
		if i == len(minusSplit)-1 {
			allMinus = append(allMinus, minusSplit[i])
		} else {
			allMinus = append(allMinus, minusSplit[i], "-")
		}
	}
	var all []string
	for i := 0; i < len(allMinus); i++ {
		plusSplit := strings.Split(allMinus[i], "plus")
		for j := 0; j < len(plusSplit); j++ {
			if j == len(plusSplit)-1 {
				all = append(all, plusSplit[j])
			} else {
				all = append(all, plusSplit[j], "+")
			}
		}
	}
	var result int
	for i := 0; i < len(all); i++ {
		if i == 0 {
			num := getValue(strNums, all[i])
			result = num
		}
		if all[i] == "+" && i < len(all)-1 {
			num := getValue(strNums, all[i+1])
			result += num
			i++
		}
		if all[i] == "-" && i < len(all)-1 {
			num := getValue(strNums, all[i+1])
			result -= num
			i++
		}
	}
	numStr := map[int]string{
		0: "zero",
		1: "one",
		2: "two",
		3: "three",
		4: "four",
		5: "five",
		6: "six",
		7: "seven",
		8: "eight",
		9: "nine",
	}
	strNumber := strconv.Itoa(result)
	var strResult string
	for _, letter := range strNumber {
		n := int(letter - '0')
		key := mapkey(numStr, n)
		strResult += key
	}

	return strResult
}

func getValue(m map[string]int, key string) int {
	if _, exist := m[key]; exist {
		return m[key]
	}
	return 0
}

func mapkey(m map[int]string, key int) string {
	if _, exist := m[key]; exist {
		return m[key]
	}
	return ""
}
