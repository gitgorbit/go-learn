package main

import "testing"

func TestStrChallenge(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "One Operation with +",
			args: args{
				str: "twoplusone", // 2 + 1
			},
			want: "three",
		},
		{
			name: "One Operation with -",
			args: args{
				str: "twominusone", // 2 - 1
			},
			want: "one",
		},
		{
			name: "Many Operations with + and - with first -",
			args: args{
				str: "twominusoneplusfive", // 2 - 1 + 5
			},
			want: "six",
		},
		{
			name: "Many Operations with + and - with first +",
			args: args{
				str: "sixminustwoplusone", // 6 - 2 + 1
			},
			want: "five",
		},
		{
			name: "Many Operations with only +",
			args: args{
				str: "twoplusthreeplusfour", // 2 + 3 + 4
			},
			want: "nine",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StrChallenge(tt.args.str); got != tt.want {
				t.Errorf("StrChallenge() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkStrChallenge(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		StrChallenge("twoplusone")
	}
}
func BenchmarkStrChallengeWithSplit(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		StrChallengeWithSplit("twoplusone")
	}
}
