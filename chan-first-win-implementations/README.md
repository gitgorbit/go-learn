Both examples work same. The main go routine will end when at least one goroutine finished.
It could wait for all, but at least one goroutine is awaited
Sleep is added to simulate that many of jobs can be finished.
Without sleep, it will wait for at least one gouroutine to finish

The first response wins: 
https://go101.org/article/channel-use-cases.html