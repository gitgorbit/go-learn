package main

import (
	"fmt"
)

func main() {
	// with unbuffered channel - at least one goroutine will finish
	done := make(chan struct{})

	go func() {
		fmt.Println("first")
		done <- struct{}{}
	}()

	go func() {
		fmt.Println("second")
		done <- struct{}{}
	}()

	go func() {
		fmt.Println("third")
		done <- struct{}{}
	}()

	<-done
	fmt.Println("done")
}
