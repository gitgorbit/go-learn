package main

import (
	"fmt"
)

func main() {
	done := make(chan struct{})
	fn1 := func() {
		fmt.Println("first")
		// time.Sleep(time.Second * 1)
	}
	fn2 := func() {
		fmt.Println("second")
		// time.Sleep(time.Second * 2)
	}
	fn3 := func() {
		fmt.Println("third")
		// time.Sleep(time.Second * 3)
	}
	fn := func(done chan struct{}, fn func()) {
		fn()
		done <- struct{}{}
	}

	go fn(done, fn1)
	go fn(done, fn2)
	go fn(done, fn3)
	<-done
	fmt.Println("done")

}
