package main

import (
	"fmt"
)

func main() {
	done := make(chan struct{})
	go func() {
		fmt.Println("first")
		// time.Sleep(time.Second * 1)
		done <- struct{}{}
	}()
	go func() {
		fmt.Println("second")
		// time.Sleep(time.Second * 2)
		done <- struct{}{}
	}()
	go func() {
		fmt.Println("third")
		// time.Sleep(time.Second * 3)
		done <- struct{}{}
	}()

	<-done
	go func() {
		<-done
		<-done
		close(done)
	}()

	fmt.Println("end")
}
