package main

import (
	"fmt"
)

func main() {
	jobs := make(chan int)

	go func() {
		defer close(jobs)
		for j := 1; j <= 3; j++ {
			jobs <- j
			fmt.Println("sent job", j)
		}
	}()

	// for r := range jobs {
	// 	fmt.Println("received job", r)
	// }

	// Alternatevily iterate in infinite loop with check if channel is empty and return statement to terminate a read
	for {
		j, ok := <-jobs
		if ok {
			fmt.Println(j)
		} else {
			return
		}
	}

	fmt.Println("---------- sent all job ------------")
}
