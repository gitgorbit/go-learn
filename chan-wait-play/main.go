package main

import "fmt"

func main() {

	c := make(chan struct{})

	go func() {
		fmt.Println("first") // order is important
		c <- struct{}{}      // should be at the end

	}()

	fmt.Println("waiting...")
	<-c
}
