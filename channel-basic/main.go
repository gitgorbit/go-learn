package main

import "fmt"

func main() {
	s := []int{1, 4, 5, 6}
	c := make(chan int)

	go sum(s, c)

	result := <-c

	fmt.Println(result)
}

// variation to use without `go` keyword
// Buffered channel should be used, since buffered channels are blocking

// func main() {
// 	s := []int{1, 4, 5, 6}
// 	c := make(chan int, 1)

// 	sum(s, c)

// 	result := <-c

// 	fmt.Println(result)
// }

func sum(nums []int, result chan<- int) {
	r := 0
	for i := 0; i < len(nums); i++ {
		r += nums[i]
	}
	result <- r
}
