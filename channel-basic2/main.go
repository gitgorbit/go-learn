package main

import "fmt"

func main() {
	// when not using `go` keyword, channel needs to be buffered
	ch := make(chan int, 1)

	ch <- 2
	fmt.Println(<-ch)
}
