package main

import (
	"fmt"
	"math/rand"
	"time"
)

// https://go101.org/article/channel-use-cases.html
func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int, 2)
	go longRequest(a)
	go longRequest(a)
	r := sumSquares(<-a, <-a)
	fmt.Println(r)
}

func longRequest(r chan<- int) {
	time.Sleep(time.Second * 1)
	r <- rand.Int()
}

func sumSquares(a, b int) int {
	return a*a + b*b
}
