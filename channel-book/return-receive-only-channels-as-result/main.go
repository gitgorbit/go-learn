package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// https://go101.org/article/channel-use-cases.html
func main() {
	// 1. First way
	// rand.Seed(time.Now().UnixNano())
	// a, b := longTimeRequest(), longTimeRequest()
	// v1, v2 := <-a, <-b
	// fmt.Println(sumSquares(v1, v2))

	// 2. Second way to synchronize goroutines with two channel
	// rand.Seed(time.Now().UnixNano())
	// doneA := make(chan struct{})
	// var a <-chan int
	// go func() {
	// 	a = longTimeRequest()
	// 	doneA <- struct{}{}
	// }()

	// doneB := make(chan struct{})
	// var b <-chan int
	// go func() {
	// 	b = longTimeRequest()
	// 	doneB <- struct{}{}
	// }()

	// <-doneA
	// <-doneB
	// fmt.Println(sumSquares(<-a, <-b))

	// 3. Third way with select
	// rand.Seed(time.Now().UnixNano())
	// done := make(chan struct{}, 2)
	// var a <-chan int
	// go func() {
	// 	a = longTimeRequest()
	// 	done <- struct{}{}
	// }()

	// var b <-chan int
	// go func() {
	// 	b = longTimeRequest()
	// 	done <- struct{}{}
	// }()
	// <-done
	// <-done

	// fmt.Println(sumSquares(<-a, <-b))

	// 4. Fourth way with sync.WaitGroup
	rand.Seed(time.Now().UnixNano())
	wg := sync.WaitGroup{}
	var a <-chan int
	var b <-chan int
	wg.Add(1)
	go func() {
		a = longTimeRequest()
		b = longTimeRequest()
		wg.Done()
	}()
	wg.Wait()

	fmt.Println(sumSquares(<-a, <-b))
}

func longTimeRequest() <-chan int {
	r := make(chan int)

	go func() {
		time.Sleep(time.Second * 3)
		r <- rand.Int()
	}()
	return r
}

func sumSquares(a, b int) int {
	return a*a + b*b
}
