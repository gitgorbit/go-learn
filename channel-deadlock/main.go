package main

import (
	"context"
	"fmt"
	"time"
)

const (
	taskTimeout = 5 * time.Second
)

// TaskResponse ...
type TaskResponse struct {
	result string
	err    error
}

// Task has command to execute on runtime api, and response channel for result
type Task struct {
	command  string
	response chan TaskResponse
}

// SingleRuntime handles one runtime API
type SingleRuntime struct {
	jobs chan Task
}

func (s *SingleRuntime) Init() error {
	s.jobs = make(chan Task)
	go s.handleIncommingJobs(context.Background())
	return nil
}

func (s *SingleRuntime) handleIncommingJobs(ctx context.Context) {
	for {
		select {
		case job, ok := <-s.jobs:
			if !ok {
				return
			}
			// just sleep to simulate dial timeout
			// time.Sleep(taskTimeout)
			job.response <- TaskResponse{result: "result"}
		case <-ctx.Done():
			return
		}
	}
}

func (s *SingleRuntime) executeRaw(command string, retry int) (string, error) {
	response := make(chan TaskResponse)
	task := Task{
		command:  command,
		response: response,
	}
	s.jobs <- task
	rsp := <-response
	if rsp.err != nil && retry > 0 {
		retry--
		return s.executeRaw(command, retry)
	}
	return rsp.result, rsp.err
}

func main() {
	client := &SingleRuntime{}
	if err := client.Init(); err != nil {
		fmt.Printf("init single runtime failed, err %s", err.Error())
	}
	result1, err := client.executeRaw("test command1", 1)
	if err != nil {
		fmt.Printf("executeRaw command1 failed, err %s\n", err.Error())
	} else {
		fmt.Printf("result is %s", result1)
	}

	result2, err := client.executeRaw("test command2", 1)
	if err != nil {
		fmt.Printf("executeRaw command2 failed, err %s\n", err.Error())
	} else {
		fmt.Printf("result is %s", result2)
	}

	result3, err := client.executeRaw("test command2", 1)
	if err != nil {
		fmt.Printf("executeRaw command2 failed, err %s\n", err.Error())
	} else {
		fmt.Printf("result is %s", result3)
	}
}
