package main

import (
	"fmt"
	"sync"
)

// https://stackoverflow.com/questions/45020481/range-a-channel-finishes-with-deadlock

func main() {
	ConcurrentFrequencyWithCloseChannel([]string{"amel"})

}

type FreqMap map[rune]int

func Frequency(s string) FreqMap {
	m := FreqMap{}
	for _, r := range s {
		m[r]++
	}
	return m
}

// Works with sync.WaitGroup
func ConcurrentFrequency(l []string) FreqMap {
	ch := make(chan FreqMap, len(l))
	wg := sync.WaitGroup{}
	for _, s := range l {
		wg.Add(1)
		go func(s string) {
			defer wg.Done()
			ch <- Frequency(s)
		}(s)
	}
	wg.Wait()
	close(ch)
	m := FreqMap{}
	for c := range ch {
		fmt.Println("channel:", c)
		for k, v := range c {
			m[k] += v
		}
	}
	return m
}

// try with channel
func ConcurrentFrequencyWithCloseChannel(l []string) {
	ch := make(chan FreqMap)
	done := make(chan struct{})

	for i, s := range l {
		go func(s string, i int) {
			fmt.Println("i: ", i)
			defer func() {
				done <- struct{}{}
				close(ch)
			}()
			ch <- Frequency(s)

		}(s, i)
	}

	<-done
	for c := range ch {
		fmt.Println("channel:", c)
	}
	fmt.Println("done")
}
