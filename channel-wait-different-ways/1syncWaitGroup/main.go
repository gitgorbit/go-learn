package main

import (
	"fmt"
	"os"
	"runtime/trace"
	"strconv"
	"sync"
)

//go tool trace -http=127.0.0.1:8003 trace.out
func traceFn() {
	//https://making.pusher.com/go-tool-trace/
	f, err := os.Create("trace.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()
}

func main() {
	traceFn()

	wg := sync.WaitGroup{}

	errs := make(chan string, 3)
	for i := 0; i < 3; i++ {
		wg.Add(1)

		go func(x int) {
			errs <- "test-" + strconv.Itoa(x)
			wg.Done()
		}(i)
	}
	wg.Wait() //ensure that above goroutines has finised

	// <-errs
	close(errs)
	for err := range errs {
		fmt.Println(err)
	}
}
