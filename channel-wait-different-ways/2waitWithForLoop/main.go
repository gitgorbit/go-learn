package main

import (
	"fmt"
	"strconv"
)

func main() {

	errs := make(chan string, 3)
	for i := 0; i < 3; i++ {
		go func(x int) {
			//This will cause a BUG - fatal error: all goroutines are asleep - deadlock!
			if x == 0 {
				return
			}
			errs <- "test-" + strconv.Itoa(x)
		}(i)
	}

	// read from chanel
	// This is error prone, if there are less errors than 3
	// len(errs) can't be used here
	for i := 0; i < 3; i++ {
		err := <-errs
		fmt.Println(err)
	}
}
