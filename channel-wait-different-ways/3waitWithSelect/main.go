package main

import (
	"fmt"
	"strconv"
)

func main() {

	errs := make(chan string)
	for i := 0; i < 3; i++ {
		go func(x int) {
			// if x == 0 {
			// 	return
			// }
			errs <- "test-" + strconv.Itoa(x)
		}(i)
	}

	for val := range errs {
		fmt.Println(val)
	}
	// for {
	// 	select {
	// 	case err := <-errs:
	// 		fmt.Println(err)
	// 		// will run forever
	// 	default:
	// 	}
	// }
}
