package main

// When using channels as function parameters, you can
// specify if a channel is meant to only send or receive
// values. This specificity increases the type-safety of
// the program.
import "fmt"

func main() {
	chan1 := make(chan string, 1)
	chanForSending(chan1, "passed message")

	chan2 := make(chan string, 1)
	chanForRecieving(chan1, chan2)

	fmt.Println(<-chan2)
}

// This `chanForSending` function only accepts a channel for sending
// values. It would be a compile-time error to try to
// receive on this channel.
func chanForSending(name chan<- string, msg string) {
	name <- msg
}

// The `chanForRecieving` function accepts one channel for receives
// (`pings`) and a second for sends (`pongs`).
func chanForRecieving(name <-chan string, fullname chan<- string) {
	msg := <-name
	fullname <- msg
}
