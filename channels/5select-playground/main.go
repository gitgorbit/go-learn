package main

import "fmt"

func main() {
	c1 := make(chan string)
	c2 := make(chan string)

	go func() {
		defer close(c1)
		c1 <- "msg1"
	}()

	// go func() {
	// 	// closing on closed channel(c1), yields `panic`
	// 	defer close(c1)
	// 	c1 <- "msg1 copy"
	// }()

	go func() {
		// defer close(c2)
		c2 <- "msg2"
	}()

	for c := range c1 {
		fmt.Println(c)
	}

	select {
	case msg1 := <-c1:
		fmt.Println("msg1 recieved", msg1)
	case msg2 := <-c2:
		fmt.Println("msg2 recieved", msg2)
	default:
		fmt.Println("no value recieved")
	}
	// for {
	// }

	// for {
	// 	fmt.Println("endless for")
	// 	continue
	// }
}
