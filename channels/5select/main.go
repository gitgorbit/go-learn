package main

import (
	"fmt"
	"time"
)

// Go's _select_ lets you wait on multiple channel
// operations. Combining goroutines and channels with
// select is a powerful feature of Go.
func main() {

	// For our example we'll select across two channels.
	c1 := make(chan string)
	c2 := make(chan string)

	// Each channel will receive a value after some amount
	// of time, to simulate e.g. blocking RPC operations
	// executing in concurrent goroutines.
	go func() {
		// Dont use Sleep, JUST FOR DEMO, use close(channel) instead
		// close(c2)
		time.Sleep(1 * time.Second)
		c1 <- "msg1"
	}()

	go func() {
		time.Sleep(1 * time.Second)
		c2 <- "msg2"
	}()

	// We'll use `select` to await both of these values
	// simultaneously, printing each one as it arrives.
	for i := 0; i < 2; i++ {
		select {
		case msg1 := <-c1:
			fmt.Println("msg1 recieved", msg1)
		case msg2 := <-c2:
			fmt.Println("msg2 recieved", msg2)
			// default:
			// 	fmt.Println("no value recieved")
		}
	}
}
