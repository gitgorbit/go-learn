package main

import (
	"fmt"
	"time"
)

func main() {
	c1 := make(chan string, 1)
	go func() {
		time.Sleep(time.Second * 2)
		c1 <- "msg"
	}()

	select {
	case res := <-c1:
		fmt.Println("recieved", res)
	case <-time.After(1 * time.Second):
		fmt.Println("timeout for first")
	}

	c2 := make(chan string, 1)
	go func() {
		time.Sleep(time.Second * 1)
		c2 <- "msg2"
	}()

	select {
	case res := <-c2:
		fmt.Println("recieved", res)
	case <-time.After(time.Second * 3):
		fmt.Println("timeout for second job")
	}

}
