package main

import (
	"fmt"
	"os"
	"runtime/trace"
)

func traceFn() {
	//https://making.pusher.com/go-tool-trace/
	f, err := os.Create("trace.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()
}

// https://gobyexample.com/non-blocking-channel-operations
func main() {
	// traceFn()

	messages := make(chan string)
	signals := make(chan bool)
	quit := make(chan struct{})

	go func() {
		fmt.Println("inside go routine")
		// messages <- "test"
		quit <- struct{}{}
		// 2 close inside goroutine
		// close(quit)
	}()

	// 1
	<-quit
	close(quit)

	select {
	// non-blocking receive
	case msg := <-messages:
		fmt.Println("recieved", msg)
	case <-quit:
		fmt.Println("quit")
	// 1
	default:
		fmt.Println("nothing recieved")
	}

	msg := "hi"
	select {
	// non-blocking send
	case messages <- msg:
		fmt.Println("sent message", msg)
	default:
		fmt.Println("no message sent")
	}

	select {
	case msg := <-messages:
		fmt.Println("received", msg)
	case sig := <-signals:
		fmt.Println("received", sig)
	default:
		fmt.Println("no activity")
	}
}
