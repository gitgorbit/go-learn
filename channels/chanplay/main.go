package main

import (
	"fmt"
)

/******************************************************************/

// If the channel is unbuffered, the sender blocks until the receiver has received the value.
// If the channel has a buffer, the sender blocks only until the value has been copied to the buffer;
// if the buffer is full, this means waiting until some receiver has retrieved a value.

/******************************************************************/

// In Go, channel is used with another goroutine.
// So, unbuffered channel can work in this situation.
// `go func` is recieving goroutine

// func main(){
// 	c := make(chan int)
//     go func() {
//         fmt.Println("received:", <-c)
//     }()
//     c <- 1
// }

func main() {

	w := &Watcher{}
	// need buffered channel
	w.update = make(chan struct{}, 1)
	// defer close(w.update)

	// recieve a value
	w.update <- struct{}{}

	// blocking a channel
	<-w.update

	w.notifyChanged()

	// for n := range w.update {
	// 	fmt.Println(n)
	// }

	/*************************/
	// without go routine, this works
	// w.notifyChanged()

}

type Watcher struct {
	update chan struct{}
}

func (w *Watcher) notifyChanged() {
	select {
	case w.update <- struct{}{}:
		fmt.Println("updated")
	default:
		fmt.Println("returned")
		return
	}
}
