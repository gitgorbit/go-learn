package main

import (
	"fmt"
	"sync"
	"time"
)

//https://dave.cheney.net/2013/04/30/curious-channels
func main() {
	finish := make(chan bool)
	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		for {
			fmt.Println("loop")
			select {
			case <-time.After(1 * time.Second):
				fmt.Println("timer")
			case <-finish:
				fmt.Println("done")
				// return
				// default:
				// 	fmt.Println("default")
			}
			wg.Done()
		}
	}()

	// <-finish
	//send the close signal
	finish <- true
	wg.Wait()

	fmt.Println("finished")
}
