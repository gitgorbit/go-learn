package main

import (
	"fmt"
)

func main() {

	fmt.Println("start")
	// without channel buffering (initialized with number)
	// messages are not printed in expected order
	message := make(chan string, 3)

	go func(message chan string) {
		// without close error occured:
		// fatal error: all goroutines are asleep - deadlock!
		defer close(message)
		message <- "gorouine 1"
		message <- "goroutine 2"
		message <- "goroutine 3"

		fmt.Println("print")
	}(message)

	for m := range message {
		fmt.Println(m)
	}
	fmt.Println("done")

}
