package main

import (
	"fmt"
)

// https://www.programming-books.io/essential/go/signaling-channel-with-chan-struct-f5daa999f5134b9ba9f2d69916df292a

// Sometimes you don’t want to send a value over a channel but use it only as a way to signal an event.
// In those cases use chan struct{} to document the fact that the value sent over a channel has no meaning.
// Signaling channel is often used as a way to tell goroutines to finish.
func worker(ch chan int, chQuit chan struct{}) {
	counter := 0
	for {
		counter++
		fmt.Println("counter: ", counter)
		select {
		case v := <-ch:
			fmt.Println("Got value: ", v)
		case <-chQuit:
			fmt.Println("Signalled on quit channel. Finishing")
			chQuit <- struct{}{}
			return
			// default:
			// 	fmt.Println("run forever")
		}
	}
}
func main() {

	ch := make(chan int)
	chQuit := make(chan struct{})
	//go worker(ch, chQuit)
	// send value to channel
	// ch <- 3
	// chQuit <- struct{}{}

	// // wait to be signalled back by the worker
	// <-chQuit

	for i := 0; i < 5; i++ {
		go worker(ch, chQuit)
		ch <- i
	}
	chQuit <- struct{}{}
	<-chQuit

	// close(chQuit)
}
