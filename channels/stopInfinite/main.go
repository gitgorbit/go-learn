package main

import (
	"fmt"
)

// https://stackoverflow.com/questions/6807590/how-to-stop-a-goroutine

// func main() {
// 	done := make(chan struct{})

// 	d := func(c chan struct{}) {
// 		select {
// 		case <-done:
// 			fmt.Println("done")
// 			return
// 		default:
// 		}
// 		fmt.Println("work in progress")
// 	}

// 	go func() {
// 		for {
// 			fmt.Println("print")
// 			done <- struct{}{}
// 			// select {
// 			// case <-done:
// 			// 	return
// 			// default:
// 			// }
// 			d(done)
// 			// fmt.Print("work in progress")
// 		}
// 	}()

// 	// time.Sleep(time.Second * 1)
// 	<-done
// 	close(done)
// }

func main() {
	done := make(chan struct{})
	go func() {
		for {
			select {
			case <-done:
				fmt.Println("done")
				return
			default:
				fmt.Println("default")
			}
			// fmt.Println("runn")
			// time.Sleep(time.Second * 1)
		}
	}()
	done <- struct{}{}
	// close(done)
}
