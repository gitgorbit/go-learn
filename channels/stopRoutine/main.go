package main

import (
	"fmt"
	"os"
	"runtime/trace"
	"time"
)

func traceFn() {
	//https://making.pusher.com/go-tool-trace/
	f, err := os.Create("trace.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()
}

func main() {
	traceFn()
	fmt.Println("demo")
	stop := make(chan struct{})
	go func() {
		for {
			select {
			case <-time.After(time.Second):
				fmt.Println("tick")
			case <-stop:
				fmt.Println("return")
				// break will not stop routine. Use return instead
				// break
				return
			default:
				fmt.Println("default")
			}
			fmt.Println("INSIDE ---------------------")
		}
	}()
	fmt.Println("outside")

	// time.Sleep(1 * time.Second)
	stop <- struct{}{}
	// time.Sleep(1 * time.Second)

	// stop := make(chan struct{})
	// go func() {
	// ForLoop:
	// 	for {
	// 		select {
	// 		case <-time.After(time.Second):
	// 			fmt.Println("tick")
	// 		case <-stop:
	// 			fmt.Println("stopping")
	// 			break ForLoop
	// 		}
	// 	}
	// 	fmt.Println("stopped")
	// }()
	// time.Sleep(3 * time.Second)
	// stop <- struct{}{}
	// time.Sleep(3 * time.Second)

}
