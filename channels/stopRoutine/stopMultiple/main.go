package main

// https://www.godesignpatterns.com/2014/04/exiting-multiple-goroutines-simultaneously.html
func main() {
	// shutdown := make(chan struct{})
	// done := make(chan int)

	// for i := 0; i < 5; i++ {
	// 	i := i
	// 	go func() {
	// 		select {
	// 		case <-shutdown:
	// 			done <- i
	// 		}
	// 	}()
	// }

	// close(shutdown)
	// for i := 0; i < 5; i++ {
	// 	fmt.Println(<-done)
	// }
	shutdown := make(chan struct{})
	done := make(chan int)

	for i := 0; i < 5; i++ {
		i := i
		go func() {
			select {
			case <-shutdown:
				done <- i
			}
		}()
	}
}
