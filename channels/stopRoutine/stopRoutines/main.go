package main

import (
	"fmt"
	"os"
	"runtime/trace"
	"sync"
)

func traceFn() {
	//https://making.pusher.com/go-tool-trace/
	f, err := os.Create("trace-infinite.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()
}

func main() {
	// traceFn()

	w := &Watcher{
		shutdownCh: make(chan struct{}),
	}

	go func() {
		w.run()
		// <-w.shutdownCh
		close(w.shutdownCh)
		w.wg.Done()
	}()
	<-w.shutdownCh
	// w.shutdownCh <- struct{}{}

}

func (w *Watcher) run() {
	fmt.Println("run")

	// cilj je zaustaviti ove goroutine
	// na consul-haproxy projektu, u TestSetup ima sd.Shutdown("test end")
	// koji zavrsava program
	w.wg.Add(3)
	go w.task1()
	go w.task2()
	go w.task3()
	w.wg.Wait()
}

func (w *Watcher) task1() {
	for {
		select {
		case msg := <-w.shutdownCh:
			fmt.Println(msg)
			return
		default:
		}
		w.counter++
		fmt.Println("task1", w.counter)
		// w.notifyShutdownCh()
	}
}

func (w *Watcher) task2() {
	for {
		select {
		case msg := <-w.shutdownCh:
			fmt.Println(msg)
			return
		default:
		}
		w.counter++
		fmt.Println("task2", w.counter)
		// w.notifyShutdownCh()
	}
}

func (w *Watcher) task3() {
	for {
		select {
		case msg := <-w.shutdownCh:
			fmt.Println(msg)
			return
		default:
		}
		w.counter++
		fmt.Println("task3", w.counter)
		// w.notifyShutdownCh()
	}
}

type Watcher struct {
	shutdownCh chan struct{}
	wg         sync.WaitGroup
	counter    int
}

func (w *Watcher) notifyShutdownCh() {
	select {
	case msg := <-w.shutdownCh:
		fmt.Println(msg)
		return
	default:
	}
}
