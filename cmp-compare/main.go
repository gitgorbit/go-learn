package main

import (
	"fmt"

	"github.com/google/go-cmp/cmp"
)

type Runtime struct {
	Name string
	Id   int
}

func main() {

	r1 := &Runtime{Name: "Amel"}
	r2 := &Runtime{}

	fmt.Println(cmp.Equal(*r1, *r2))

	fmt.Println(*r1, *r2)

}
