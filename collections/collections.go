package main

import "fmt"

func main() {
	data := []int{1, 3, 56, 6, 5, 7}

	fmt.Println(data[1:], data[:1])

	result := fmt.Sprintf("set-query %s %s", "amel", "elma")

	fmt.Println(result)

}
