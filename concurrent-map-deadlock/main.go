package main

import (
	"sync"
	"time"
)

func main() {
	rwe := Rwexample{
		counter: make(map[int]int),
	}
	var i int = 2
	go func(i int) {
		for {
			i++
			rwe.Increase(i)
		}
	}(i)

	time.Sleep(time.Second * 2)
	r := rwe.Read(11)
	println(r)
}

type Rwexample struct {
	mu      sync.RWMutex
	counter map[int]int
}

func (r *Rwexample) Increase(key int) {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.counter[key] = key
}

func (r *Rwexample) Read(key int) int {
	r.mu.Lock()
	defer r.mu.Unlock()
	return r.counter[key]
}
