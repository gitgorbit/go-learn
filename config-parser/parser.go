package main

import (
	"log"

	parser "github.com/haproxytech/config-parser"
	"github.com/haproxytech/config-parser/errors"
	"github.com/haproxytech/config-parser/types"
)

func main() {
	p := parser.Parser{}
	err := p.LoadData("/etc/haproxy/haproxy.cfg")
	log.Println(err)
	log.Println(p.String())

	// {
	// 	data, _ := p.Get(parser.Comments, parser.CommentsSectionName, "# _version", true)
	// 	if err == errors.ErrFetch {
	// 		log.Panicln("we have an fetch error !!")
	// 	}
	// 	ver, _ := data.(*types.Int64C)
	// 	ver.Value = ver.Value + 1
	// }

	{
		p.Set(parser.Frontends, "http", "option forwardfor", types.OptionForwardFor{})
	}
	{
		// test forwardfor
		data, err := p.Get(parser.Backends, "secondary", "option forwardfor")
		if err != nil {
			log.Panicln(err)
		}
		val := data.(*types.OptionForwardFor)
		log.Println(val, "amel")
	}

	{
		// test httpchk
		data, err := p.GetOne(parser.Backends, "secondary", "option httpchk")
		if err != nil {
			log.Panicln(err)
		}
		val := data.(*types.OptionHttpchk)
		log.Println(val)
	}

	{
		data, err := p.Get(parser.Global, parser.GlobalSectionName, "stats socket")
		if err != nil {
			log.Panicln(err)
		}
		val, _ := data.([]types.Socket)
		log.Println(val[0])
		val[0].Path = "$PWD/haproxy-runtime-api.1.sock"
		log.Println(val[0])
	}

	{
		data, err := p.Get(parser.Global, parser.GlobalSectionName, "daemon")
		log.Println(data, err)
		if err == errors.ErrFetch {
			log.Panicln("we have an fetch error !!")
		}
		//remove it
		p.Set(parser.Global, parser.GlobalSectionName, "daemon", nil)
	}

	{
		datar, err := p.Get(parser.Resolvers, "ns1", "nameserver")
		if err == nil {
			ns := datar.([]types.Nameserver)
			log.Println(ns[0].Name, ns[0].Address)
			log.Println(ns[1].Name, ns[1].Address)
			ns[1].Name = "hahaha"
			ns[0].Address = "0.0.0.0:8080"
		}
		datar, err = p.Get(parser.Resolvers, "ns1", "nameserver")
		if err == nil {
			ns := datar.([]types.Nameserver)
			log.Println(ns[0].Name, ns[0].Address)
			log.Println(ns[1].Name, ns[1].Address)
		}
	}

	{
		log.Println("nbproc ==================================================")
		data, err := p.Get(parser.Global, parser.GlobalSectionName, "nbproc")
		if err != nil {
			log.Println(err)
		} else {
			d := data.(*types.Int64C)
			log.Println(d.Value)
			d.Value = 5
		}
	}

	// p.Save(configFilename)
}
