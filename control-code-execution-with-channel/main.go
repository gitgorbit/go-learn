package main

import "fmt"

func main() {
	a, b := make(chan struct{}), make(chan struct{})
	close(a)
	close(b)

	x, y := 0, 0
	f1 := func() { x++ }
	f2 := func() { y++ }

	for i := 0; i < 10000; i++ {
		select {
		case <-a:
			f1()
		case <-a:
			f1()
		case <-b:
			f2()
		}
	}

	fmt.Println(x / y)
}
