package main

import "fmt"

func main() {
	//=================================================================================
	// 1. way
	a := []int{2, 3, 4}
	var b [3]int // it has to have capacity, otherwise copy will not work

	dst := b[:]
	copy(dst, a)
	fmt.Println("b before", b)

	dst[0] = 10
	fmt.Println("dst", dst)
	fmt.Println("b after", b)

	//=================================================================================

	// 2. way is to use append
	e := []int{2, 3, 4}
	var c []int
	c = append(c, e...)
	// changes to `e` will not be propagated
	e[0] = 15
	fmt.Println("c", c)

	//=================================================================================

	// 3. way is to use append
	d := []int{2, 3, 4}
	dst2 := make([]int, len(d), cap(d)+1)
	copy(dst2, d)
	fmt.Println("dst2", dst2)
}
