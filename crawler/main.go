package main

import (
	"fmt"
	"sync"
)

type Fetcher interface {
	// Fetch returns the body of URL and
	// a slice of URLs found on that page.
	Fetch(url string) (body string, urls []string, err error)
}

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func Crawl(wg *sync.WaitGroup, url string, depth int, fetcher Fetcher, cache *UrlCache, results *Results) {
	defer wg.Done()

	if depth <= 0 || !cache.AtomicSet(url) {
		return
	}

	body, urls, err := fetcher.Fetch(url)
	if err != nil {
		results.Error <- err
		return
	}

	results.Data <- [2]string{url, body}

	for _, url := range urls {
		wg.Add(1)
		go Crawl(wg, url, depth-1, fetcher, cache, results)
	}
}

func main() {
	var wg sync.WaitGroup
	cache := NewUrlCache()

	results := NewResults()
	defer results.Close()

	wg.Add(1)
	go Crawl(&wg, "http://golang.org/", 4, fetcher, cache, results)
	go results.Read()
	wg.Wait()
}

// Results defines channels which yield results for a single crawled URL.
type Results struct {
	Data  chan [2]string // url + body.
	Error chan error     // Possible fetcher error.
}

func NewResults() *Results {
	return &Results{
		Data:  make(chan [2]string, 1),
		Error: make(chan error, 1),
	}
}

func (r *Results) Close() error {
	close(r.Data)
	close(r.Error)
	return nil
}

// Read reads crawled results or errors, for as long as the channels are open.
func (r *Results) Read() {
	for {
		select {
		case data := <-r.Data:
			fmt.Println(">", data)

		case err := <-r.Error:
			fmt.Println("e", err)
		}
	}
}

// UrlCache defines a cache of URL's we've already visited.
type UrlCache struct {
	sync.Mutex
	data map[string]struct{} // Empty struct occupies 0 bytes, whereas bool takes 1 bytes.
}

func NewUrlCache() *UrlCache {
	return &UrlCache{
		data: make(map[string]struct{}),
	}
}

// AtomicSet sets the given url in the cache and returns false if it already existed.
//
// All within the same locked context. Modifying a map without synchronisation is not safe
// when done from multiple goroutines. Doing a Exists() check and Set() separately will
// create a race condition, so we must combine both in a single operation.
func (c *UrlCache) AtomicSet(url string) bool {
	c.Lock()
	_, ok := c.data[url]
	c.data[url] = struct{}{}
	c.Unlock()
	return !ok
}

// fakeFetcher is Fetcher that returns canned results.
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
	body string
	urls []string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}
	return "", nil, fmt.Errorf("not found: %s", url)
}

// fetcher is a populated fakeFetcher.
var fetcher = fakeFetcher{
	"http://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"http://golang.org/pkg/",
			"http://golang.org/cmd/",
		},
	},
	"http://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"http://golang.org/",
			"http://golang.org/cmd/",
			"http://golang.org/pkg/fmt/",
			"http://golang.org/pkg/os/",
		},
	},
	"http://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
	"http://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
}
