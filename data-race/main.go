package main

import (
	"fmt"
	"sync"
)

// run this code with to see data race: `go run -race data-race/main.go`
func main() {
	done := make(chan bool)
	m := make(map[string]string)
	m["name"] = "world"

	var mu sync.Mutex
	go func() {
		mu.Lock()
		m["name"] = "data race"
		mu.Unlock()
		done <- true
	}()
	// time.Sleep(time.Second * 1)
	mu.Lock()
	fmt.Println("Hello,", m["name"])
	mu.Unlock()
	<-done
}
