package main

import (
	"fmt"
	"log"
	"os"
	"runtime/trace"
)

func traceFn() {
	//https://making.pusher.com/go-tool-trace/
	f, err := os.Create("trace.out")
	if err != nil {
		log.Fatalf("failed to create trace output file: %v", err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Fatalf("failed to close trace file: %v", err)
		}
	}()

	if err := trace.Start(f); err != nil {
		log.Fatalf("failed to start trace: %v", err)
	}
	defer trace.Stop()
}
func main() {
	traceFn()

	//defer will be called at the end of function
	defer sayGoodBye()

	ch := make(chan bool)
	go sayHello(ch)

	select {
	case v := <-ch:
		fmt.Println("recieved a value", v)
		return
	default:
		fmt.Println("continued")
	}
	counter()
	<-ch
}

func sayHello(done chan<- bool) {
	fmt.Println("hello")
	done <- true
}

func sayGoodBye() {
	fmt.Println("Good bye")
}

func counter() {
	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}
}
