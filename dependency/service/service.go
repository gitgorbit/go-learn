package service

import (
	"errors"
	"learn/dependency/post"
	"math/rand"
)

type PostRepository interface {
	Save(post *post.Post) (*post.Post, error)
	FindAll() ([]post.Post, error)
}

type PostService interface {
	Validate(post *post.Post) error
	Create(post *post.Post) (*post.Post, error)
	FindAll() ([]post.Post, error)
}

type service struct{}

var (
	repo PostRepository
)

func NewPostService(repository PostRepository) PostService {
	repo = repository
	return &service{}
}

func (*service) Validate(post *post.Post) error {
	if post == nil {
		err := errors.New("The post is empty")
		return err
	}
	if post.Title == "" {
		err := errors.New("The post title is empty")
		return err
	}
	return nil
}

func (*service) Create(post *post.Post) (*post.Post, error) {
	post.ID = rand.Int63()
	return repo.Save(post)
}

func (*service) FindAll() ([]post.Post, error) {
	return repo.FindAll()
}
