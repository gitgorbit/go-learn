package service

import (
	"fmt"
	"learn/dependency/post"
)

type MockedService struct {
	PostService
}

func (m *MockedService) Validate(post *post.Post) error {
	return nil
}

func (m *MockedService) Create(p *post.Post) (*post.Post, error) {
	return m.PostService.Create(p)
	// return &post.Post{ID: 12, Title: "title1", Text: "text"}, nil
}

func (m *MockedService) FindAll() ([]post.Post, error) {
	return nil, fmt.Errorf("error")
}

func NewMockedService() PostService {
	return &MockedService{PostService: NewPostService(repo)}
}
