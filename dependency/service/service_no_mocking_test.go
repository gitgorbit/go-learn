package service

import (
	"learn/dependency/post"
	"reflect"
	"testing"
)

func TestMockedService_Create(t *testing.T) {
	type args struct {
		post *post.Post
	}
	tests := []struct {
		name    string
		m       PostService
		args    args
		want    *post.Post
		wantErr bool
	}{
		{
			name:    "Test with custom mocking object",
			m:       NewMockedService(),
			args:    args{post: &post.Post{ID: 12, Title: "title1", Text: "text"}},
			want:    &post.Post{ID: 12, Title: "title1", Text: "text"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.m.Create(tt.args.post)
			if (err != nil) != tt.wantErr {
				t.Errorf("MockedService.Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MockedService.Create() = %v, want %v", got, tt.want)
			}
		})
	}
}
