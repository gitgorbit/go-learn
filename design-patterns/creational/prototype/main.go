//Definition:
//It is a creational design pattern that lets you CREATE COPIES OF OBJECTS.
//In this pattern, the responsibility of creating the clone objects is delegated to the actual object to clone.
//https://golangbyexample.com/prototype-pattern-go/

// When to Use:
// 1.We use prototype pattern when the object to be cloned creation process is complex
// i.e the cloning may involve vases of handling deep copies, hierarchical copies, etc.
// Moreover, there may be some private members too which cannot be directly accessed.

//2. A copy of the object is created instead of creating a new instance from scratch.
// This prevents costly operations involved while creating a new object such as database operation.

//3. When you want to create a copy of a new object, but it is only available to you as an interface.
// Hence you can not directly create copies of that object.

package main

import "fmt"

func main() {
	fmt.Println("dada")
}

//prototype interface
type INode interface {
	print(string)
	clone() INode
}

//file
type File struct {
	name string
}

func (f *File) print(indentation string) {
	fmt.Println(indentation + f.name + "_clone")
}

func (f *File) clone() INode {
	return &File{name: f.name}
}

//folder
type Folder struct {
	childrens []INode
	name      string
}

func (f *Folder) print(indentation string) {
	fmt.Println(indentation + f.name + "_clone")
	for _, i := range f.childrens {
		i.print(indentation + indentation)
	}
}

func (f *Folder) clone() INode {
	cloneFolder := &Folder{name: f.name}
	var tempChildrens []INode
	for _, i := range f.childrens {
		copy := i.clone()
		tempChildrens = append(tempChildrens, copy)
	}
	cloneFolder.childrens = tempChildrens
	return cloneFolder
}
