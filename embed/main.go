package main

import (
	_ "embed"
	"fmt"
)

//go:embed "text.txt"
var s string

func main() {
	fmt.Println(s)
}
