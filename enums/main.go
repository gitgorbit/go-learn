package main

import "fmt"

type Option string

const (
	Enabled  Option = "enabled"
	Disabled Option = "disabled"
)

func main() {
	t := "enabled"
	if t == string(Enabled) {
		fmt.Println("same", string(Enabled))
	}

	arr := []string{}
	fmt.Println(arr[0])
}
