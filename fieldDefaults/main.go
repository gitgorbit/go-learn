package main

import (
	"fmt"
	"reflect"
)

type MyParam struct {
	Name string `default:"amel"`
}

func main() {
	p := MyParam{}
	typ := reflect.TypeOf(p)

	if p.Name == "" {
		f, _ := typ.FieldByName("Name")
		p.Name = f.Tag.Get("default")
		fmt.Println(p.Name)
	}
}
