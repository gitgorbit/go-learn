package main

import (
	"fmt"
	"log"
	"time"
)

func main() {
	// f, err := os.OpenFile("/tmp/test.map", os.O_APPEND|os.O_WRONLY|os.O_TRUNC, 0600)
	// if err != nil {
	// 	fmt.Println("err", err.Error())
	// }

	start := time.Now()
	// for i := 0; i < 1000000; i++ {
	// 	line := fmt.Sprintf("%s-%d %s-%d %s", "key", i, "val", i, "\n")
	// 	_, err = f.WriteString(line)
	// 	if err != nil {
	// 		fmt.Printf("error writing map file: %s", err.Error())
	// 	}
	// }
	l := 100
	for i := l - 20; i < l; i++ {
		fmt.Println(i)
	}
	elapsed := time.Since(start)
	log.Printf("Writing 1M took %s", elapsed)
}
