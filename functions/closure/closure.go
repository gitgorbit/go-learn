package main

import "fmt"

func main() {

	name := "amel"
	func() {
		fmt.Println(name) //accessing var outside of anonymouse functions is what we call a closure
	}()
}
