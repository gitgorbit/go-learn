package main

import "fmt"

func main() {

	f := func(a, b int) int {
		return a + b
	}
	result := simple(f)
	fmt.Println(result)
}

func simple(fn func(a, b int) int) int {
	return fn(10, 10)
}
