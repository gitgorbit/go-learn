package main

import "fmt"

func main() {
	f := myfn()
	r := f(5, 6)
	fmt.Println(r)
}

func myfn() func(a, b int) int {
	f := func(a, b int) int {
		return a + b

	}
	return f
}
