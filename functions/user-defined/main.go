package main

import "fmt"

func main() {

	var f fn = func(a, b int) int {
		return a + b
	}

	r := f(1, 5)
	fmt.Println(r)
}

type fn func(a, b int) int
