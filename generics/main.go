package main

import "fmt"

// https://go.dev/doc/tutorial/generics

func main() {
	ints := map[string]int64{
		"first":  10,
		"second": 20,
	}
	intSum := SumInts(ints)

	floats := map[string]float64{
		"first":  10.0,
		"second": 11.0,
	}
	floatSum := SumFloats(floats)

	fmt.Printf("Sum %v and %v", intSum, floatSum)

	genericSum := SumIntOrFloats(ints)
	fmt.Printf("%v", genericSum)

	sumGenericSlice := SumGenericSlice([]int64{10, 15, 20})
	fmt.Printf("Sum generice slice %v", sumGenericSlice)
}

// SumInts adds together the values of m.
func SumInts(m map[string]int64) int64 {
	var s int64
	for _, v := range m {
		s += v
	}
	return s
}

// SumFloats adds together the values of m.
func SumFloats(m map[string]float64) float64 {
	var s float64
	for _, v := range m {
		s += v
	}
	return s
}

// first implementation with union type
// func SumIntOrFloats[K comparable, V int64 | float64](m map[K]V) V {
// 	var s V
// 	for _, v := range m {
// 		s += v
// 	}
// 	return s
// }

type Number interface {
	int64 | float64
}

func SumIntOrFloats[K comparable, V Number](m map[K]V) V {
	var s V
	for _, v := range m {
		s += v
	}
	return s
}

func SumGenericSlice[K Number](slice []K) K {
	var result K
	for _, val := range slice {
		result += val
	}
	return result
}
