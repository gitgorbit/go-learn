package main

import (
	"fmt"
)

type Student struct {
	Age int
}

// https://www.programmersought.com/article/89957407844/
func main() {
	// since Student is a value type, it is not possible to change its value
	kv := map[string]*Student{ // using pointer will fix this issue - use *Student instead of Student
		"menglu": {Age: 21},
	}
	kv["menglu"].Age = 22
	s := []Student{{Age: 21}}
	s[0].Age = 22
	fmt.Println(kv["menglu"].Age, s[0].Age)
}
