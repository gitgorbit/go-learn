package main

import (
	"fmt"
)

// Slice is reference type, while array is a value type

// https://www.programmersought.com/article/89957407844/
func main() {
	str1 := []string{"a", "b", "c"}
	str2 := str1[1:] // b, c
	str2[1] = "new"  // b, new
	fmt.Println(str1)
	str2 = append(str2, "z", "x", "y")
	fmt.Println(str2)
}

// c, new
// a, b, c
