package main

import (
	"fmt"
	"sync"
	"time"
)

// 4. Support the WaitTimeout function for the Wait function in sync.WaitGroup ()
func main() {
	wg := sync.WaitGroup{}
	c := make(chan struct{})
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(num int, close <-chan struct{}) {
			defer wg.Done()
			<-close
			fmt.Println(num)
		}(i, c)
	}

	if WaitTimeout(&wg, time.Second*5) {
		close(c)
		fmt.Println("timeout exit")
	}
	time.Sleep(time.Second * 10)
}

func WaitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	// requires handwritten code
	// requires sync.WaitGroup to support timeout function
	// If timeout expires, return true
	// If WaitGroup naturally ends, return false
	wgChan := make(chan struct{})
	go func() {
		wg.Wait()
		wgChan <- struct{}{}
	}()

	select {
	case <-wgChan:
		return false
	case <-time.After(timeout):
		return false
	}
}
