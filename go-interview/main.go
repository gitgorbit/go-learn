package main

import (
	"fmt"
	"time"
)

// 1 Write the following logic, which requires calling proc every second and ensuring that the program does not exit
func main() {
	go func() {
		// 1 Here you need to write the algorithm
		// 2 requires calling the proc function every second
		// 3 requires the program not to exit
		for {
			time.Sleep(time.Second * 1)
			func() {
				defer func() {
					if err := recover(); err != nil {
						fmt.Println("Catch exception:", err)
					}
				}()
				proc()
				fmt.Println("recover")
			}()
		}
	}()

	select {}
}

func proc() {
	panic("ok")
}
