package main

import (
	"log"
	"time"
)

func main() {
	log.SetFlags(0)

	ready, done := make(chan T), make(chan T)
	go worker(0, ready, done)
	go worker(1, ready, done)
	go worker(2, ready, done)

	// Simulate an initialization phase.
	time.Sleep(time.Second * 3 / 2)

	// 1-to-N notifications
	ready <- T{}
	ready <- T{}
	ready <- T{}

	// being N-to-1 notified
	<-done
	<-done
	<-done
}

type T = struct{}

func worker(id int, ready <-chan T, done chan<- T) {
	// block and wait for notification
	<-ready
	log.Print("worker#", id, "starts")

	//Simulate a workload
	time.Sleep(time.Second * 1)
	log.Print("worker#", id, "done")

	// notify the main goroutine (N-to-1)
	done <- T{}

}
