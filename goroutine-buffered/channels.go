package main

import (
	"fmt"
	"time"
)

func main() {
	foo()
	channel := make(chan int)
	done := make(chan struct{})
	go bgTask(channel)
	go task2()
	go func() {
		time.Sleep(time.Second * 2)
		channel <- 1
		done <- struct{}{}
	}()
	<-done
	//fmt.Println(<-channel)
}

func foo() {
	fmt.Println("foo")
}

func bgTask(c <-chan int) {
	period := 1
	ticker := time.NewTicker(time.Duration(period) * time.Second)

	for {
		select {
		case <-ticker.C:
			fmt.Println("Tick at uptimeTicker")
		case <-c:
			return
		case <-time.After(time.Duration(<-c)):
			fmt.Println("Tick at Duration")
			return
		}
	}
}

func statusUpdate() string { return "" }

func task2() {
	c := time.Tick(1 * time.Second)
	for now := range c {
		fmt.Printf("updates task2 %v %s\n", now, statusUpdate())
	}
}
