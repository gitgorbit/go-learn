package main

import "fmt"

func sum(s []int, c chan<- int) {
	sum := 0
	for _, v := range s {
		sum += v
	}

	c <- sum // send sum to channel
}

func main() {

	nums := []int{7, -7, -1, 6, 4}
	channel := make(chan int)

	go sum(nums[:len(nums)/2], channel)
	go sum(nums[len(nums)/2:], channel)

	x, y := <-channel, <-channel //receive from channel

	fmt.Println(x, y)

}
