package main

import (
	"fmt"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(1000 * time.Millisecond)
		fmt.Println(s)
	}
}

// func sayChan(d chan string, s string) {
// 	c := make(chan string, 1)
// 	for i := 0; i < 5; i++ {
// 		fmt.Println(s)
// 	}
// }
func main() {
	say("world")
	go say("hello")
	fmt.Println("s")
}
