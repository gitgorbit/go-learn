package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

type Student struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

func main() {

	body := Student{
		Name:    "abc",
		Address: "xyz",
	}

	buf := new(bytes.Buffer)
	e := json.NewEncoder(buf).Encode(body)
	if e != nil {
		log.Fatal(e)
	}
	req, _ := http.NewRequest("POST", "https://httpbin.org/post", buf)

	client := http.Client{}
	res, e := client.Do(req)
	if e != nil {
		log.Fatal(e)
	}

	defer res.Body.Close()

	fmt.Println("response Status:", res.Status)

	// Print the body to the stdout
	_, _ = io.Copy(os.Stdout, res.Body)
}
