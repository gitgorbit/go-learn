package main

import "fmt"

func main() {
	run := '\a'
	fmt.Println(run)

	str := "a"
	b := str[0]
	fmt.Println(b)

	fmt.Println(b == byte(run))

	str2 := "amel"
	for i, c := range str2 {
		fmt.Println(c, i)
		fmt.Println("")
		fmt.Printf("format %c", c)
		fmt.Println("")
		fmt.Println("byte", byte(c))
	}

	str3 := "["
	fmt.Println("str3: ", str3[0])
}
