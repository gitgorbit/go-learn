package main

import "fmt"

func main() {
	w := Watcher{
		done: make(chan struct{}),
	}

	go func() {
		w.watch()
		// <-w.done
	}()
	w.done <- struct{}{}

}

type Watcher struct {
	done chan struct{}
}

func (w Watcher) watch() {
	for {
		fmt.Println("inside for")
		select {
		case <-w.done:
			fmt.Println("done")
			return
		default:
			fmt.Println("default")
		}
	}
}
