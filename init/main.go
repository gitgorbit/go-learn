package main

import "fmt"

func main() {
	fmt.Println(x, "main")
	x++
	fmt.Println("main++", x)
}

type Integer int

var x Integer = 12

// init works before main
func init() {
	x = 15
	fmt.Println(x, "init")
}
