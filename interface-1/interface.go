package main

import (
	"fmt"
)

type Shape interface {
	calculate(x int) int
}

type Triangle struct {
	height int
}

func (triangle *Triangle) calculate(x int) int {
	return x * x
}

func main() {

	triangle := Triangle{}
	triangle.height = 12
	fmt.Println(triangle.calculate(10))

	// var shape Shape = &Triangle{}
	// result := shape.calculate(10)

	// fmt.Println(result)
}
