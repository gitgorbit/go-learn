package main

import (
	"fmt"
)

type Shaper interface {
	calculate(x int) int
}

type Triangle struct {
	height int
}

func (triangle *Triangle) calculate(x int) int {
	return triangle.height * x
}

type Square struct {
	area int
}

func (s *Square) calculate(x int) int {
	return s.area * x
}

func (s *Square) sleep(x int) int {
	return s.area * x
}

type Manager struct {
	Shaper
}

func main() {

	t := &Triangle{height: 5}
	// reader := io.Reader
	// bufio.Reader
	mt := Manager{t}
	rm := mt.calculate(10)
	fmt.Println(rm)

	s := &Square{area: 12}
	ms := Manager{s}
	rs := ms.calculate(10)

	fmt.Println(rs)

}
