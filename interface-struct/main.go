package main

import "fmt"

func main() {
	fmt.Println(struct{}{}, "empty struct")

	var struct1 struct{}
	fmt.Println(struct1, "struct1{}")

	var iStruct interface{} = struct{}{}
	fmt.Println(iStruct, "interface")

	str1 := struct1

	//interface vs struct
	//true
	fmt.Println(iStruct == str1)

	//interface vs empty struct
	//true
	fmt.Println(iStruct == struct{}{})

	//struct vs empty struct
	//true
	fmt.Println(str1 == struct{}{})

	var iString interface{} = "test"
	var str string = "test"
	//true
	fmt.Println(iString == str, "interface vs string")

}
