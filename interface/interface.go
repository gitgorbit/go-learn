package main

import (
	"fmt"
)

type Person struct {
	name string
}

type Actions interface {
	CanWalk() bool
	CanRead() bool
}

type Utility interface {
	CanSwimm(isProffesional bool) bool
}

func (person *Person) CanRead() bool {
	return true
}

func (person *Person) CanSwimm(isProffesional bool) bool {
	return isProffesional
}

func (person *Person) CanRide() bool {
	return false
}
func main() {
	person := Person{name: "amel"}
	person.CanRead()

	fmt.Println(person.CanSwimm(false))
}
