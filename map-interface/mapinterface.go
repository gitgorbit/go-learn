package main

import "fmt"

func main() {
	person := make(map[string]interface{})
	person["arm"] = false

	person["name"] = "amel"
	fmt.Println(person["arm"], person["name"])

	delete(person, "dada")
}
