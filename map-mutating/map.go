package main

import "fmt"

func main() {
	m := make(map[string]int)

	x := make(map[string]int)

	x["elma"] = 35
	fmt.Println(x["elma"])
	delete(x, "elma")

	m["amel"] = 42
	fmt.Println("The value:", m["amel"])

	m["Answer"] = 48
	fmt.Println("The value:", m["Answer"])

	delete(m, "Answer")
	fmt.Println("The value:", m["Answer"])

	v, ok := m["Answer"]
	fmt.Println("The value:", v, "Present?", ok)
}
