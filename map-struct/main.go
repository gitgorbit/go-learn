package main

import "fmt"

func main() {
	data := make(map[string]struct{})

	data["amel"] = struct{}{}

	if data["amel"] == struct{}{} {
		fmt.Println("has data")
	} else {
		fmt.Println("no data")
	}

	data = make(map[string]struct{})
	fmt.Println(len(data))

}
