package main

import "fmt"

type Vertex struct {
	Lat, Long float64
}

var m map[string]Vertex

func main() {
	m = make(map[string]Vertex)
	m["amel"] = Vertex{
		Lat:  2.8,
		Long: 2.10,
	}
	vertex := m["amel"]
	fmt.Println(m["amel"], vertex.Lat, vertex.Long)

	p := Person{}
	p.person = make(map[string]string)
	p.person["male"] = "38"
	fmt.Println(p.person["male"])
}

type Person struct {
	person map[string]string
}
