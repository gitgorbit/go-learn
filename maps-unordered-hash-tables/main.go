package main

import (
	"fmt"
	"sort"
)

func main() {
	books := make(map[int]string)
	books[1] = "1"
	books[2] = "2"
	books[3] = "3"
	books[4] = "4"

	// running multiple times gives different order of books
	// because `map` is an unordered data structure
	for i, b := range books {
		fmt.Println(i, b)
	}

	// to fix this problem, we need to sort it by keys, e.g.:
	var keys []int
	for k := range books {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	// now iterate over map by this keys:
	for _, k := range keys {
		fmt.Println("sorted: ", books[k])
	}
}
