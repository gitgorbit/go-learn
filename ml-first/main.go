package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/sjwhitworth/golearn/base"
	"github.com/sjwhitworth/golearn/evaluation"
	"github.com/sjwhitworth/golearn/knn"
)

func main() {
	basepath, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	path := filepath.Join(basepath, "iris_headers.csv")

	// irisCsv, err := os.Open(path)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// df := dataframe.ReadCSV(irisCsv)
	// //fmt.Println(df)

	// // 1. subsetting
	// head := df.Subset([]int{0, 3})
	// fmt.Println(head)

	// // 2. filtering
	// versicolorOnly := df.Filter(dataframe.F{
	// 	Colname:    " Species",
	// 	Comparator: "==",
	// 	Comparando: "Iris-versicolor",
	// })
	// fmt.Println(versicolorOnly)

	// // 3. Column Selection
	// attrFiltered := df.Select([]string{"Petal length", "Sepal length"})
	// fmt.Println(attrFiltered)

	// ML part
	// build a simple KNN classifier to determine IRIS’ species type
	// given its collection of attributes (eg. Petal length, petal width, etc).

	fmt.Println("Load our csv data")
	rawData, err := base.ParseCSVToInstances(path, true)
	if err != nil {
		panic(err)
	}

	// 1. Initialize a KNN Classifier
	fmt.Println("Initialize our KNN classifier")
	cls := knn.NewKnnClassifier("euclidean", "linear", 2)

	// 2. Training-Testing Split
	fmt.Println("Perform a training-test split")
	trainData, testData := base.InstancesTrainTestSplit(rawData, 0.50)
	// 3. Train the Classifier
	cls.Fit(trainData)

	// predict
	fmt.Println("Calculate the euclidian distance and return the most popular label")
	predictions, err := cls.Predict(testData)
	if err != nil {
		panic(err)
	}
	fmt.Println(predictions)

	// 4. Summary Metrics
	fmt.Println("Print our summary metrics")
	confusionMat, err := evaluation.GetConfusionMatrix(testData, predictions)
	if err != nil {
		panic(fmt.Sprintf("Error: %s", err.Error()))
	}
	fmt.Println(evaluation.GetSummary(confusionMat))
}
