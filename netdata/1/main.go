package main

type Text string

func (t *Text) String() string { return string(*t) }

func main() {
	// v := print(map[string]Text{}["text"].String())
	// print(v)
	v := map[string]Text{"text": Text("val")}
	print(v)
}
