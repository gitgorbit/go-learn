package main

type T struct{ _ bool }

func main() {
	print(T{true} == T{false})
}
