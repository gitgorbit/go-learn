package main

func main() {
	// Same declaration - type is same - array
	// a := [8]int{1, 2, 3, 4, 5, 6, 7, 8}
	a := [...]int{1, 2, 3, 4, 5, 6, 7, 8} // compiler will decide on number of element by calculating elements
	func(s []int) {
		ss := s[:0]
		println(ss)
		for i, v := range s[:cap(s)] {
			if v%2 == 0 {
				defer func() { ss = append(ss, i) }()
			} else {
				ss = append(ss, v)
			}
		}
	}(a[:2])
	print(a[0], a[len(a)-1])
}
