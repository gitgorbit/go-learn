package main

import "fmt"

func ArrayChallenge(arr []int) int {

	// code goes here
	return arr[0]

}

func main() {

	// do not modify below here, readline is our function
	// that properly reads in the input for you
	// fmt.Println(ArrayChallenge(readline()))
	fmt.Println(ArrayChallenge([]int{}))

}
