package main

import "fmt"

type Person struct {
	id   int
	name string
}

func main() {
	comparer := make(map[int]interface{})

	var person []Person

	person = []Person{
		Person{
			id:   1,
			name: "amel",
		},
		Person{
			id:   2,
			name: "elma",
		},
	}

	for _, p := range person {
		comparer[p.id] = nil
	}

	fmt.Println(comparer)
}
