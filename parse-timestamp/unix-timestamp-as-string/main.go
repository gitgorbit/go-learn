package main

import (
	"fmt"
	"strconv"
	"time"
)

func main() {
	i, err := strconv.ParseInt("1612827899999", 10, 64)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(i)
	t := time.Unix(i/1000, 0)
	fmt.Println(t)
}
