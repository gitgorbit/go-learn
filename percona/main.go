package main

import (
	"flag"
	"fmt"
	"net/http"
	"strings"
	"sync"
)

var workers int
var endpoints string
var urlsDone int
var urlsError int
var mu sync.RWMutex

func main() {
	flag.IntVar(&workers, "workers", 3, "number of workers")
	flag.StringVar(&endpoints, "endpoints", "one,two,three", "list of endpoints comma delimited")
	// flag.Parse()

	ends := strings.Split(endpoints, ",")
	wg := sync.WaitGroup{}
	tasks := make(chan string, len(ends))

	for _, endpoint := range ends {
		wg.Add(1)
		tasks <- endpoint
		go func() {
			defer wg.Done()
			err := worker(tasks)
			fmt.Println(err)
		}()
	}

	wg.Wait()
	//close(tasks) // don't need to close a channel
	mu.RLock()
	fmt.Printf("work done. successfully=%d errors=%d", urlsDone, urlsError)
	mu.RUnlock()
}

func worker(urls chan string) error {
	for msg := range urls {
		_, err := http.Get(msg)
		if err != nil {
			mu.Lock()
			urlsError += 1
			mu.Unlock()
			return err
		} else {
			mu.Lock()
			urlsDone += 1
			mu.Unlock()
		}
	}
	return nil
}
