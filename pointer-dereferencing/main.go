package main

import "fmt"

type Client struct {
	name string
}

type Person struct {
	name   string
	client *Client
}

func main() {

	// The goal is to test whether person.name changes
	person := Person{name: "elma", client: &Client{name: "amel"}}

	addressed := &person

	fmt.Print("\n")
	fmt.Println(person.name, person.client.name)
	fmt.Println(addressed, addressed.client.name)

	addressed.name = "NEW"
	fmt.Println(addressed.name, person.name)

	addressed.client.name = "MYCLIENT"
	fmt.Println(addressed.client.name, person.client.name)

	// packageName := "fmt"
	// function := "Sprintf()"

	// output := fmt.Sprintf("%s %s", packageName, function)
	// fmt.Print("output", output)

	// var str *string = ""
	// l := strings.Fields(str)
	// fmt.Println("str", len(l))

}
