package main

import "fmt"

func main() {
	e := Employee{}
	e.SetName("amel")
	fmt.Println(e.Firstname)

	e.SetName("elma")
	fmt.Println(e.Firstname)
}

type Employee struct {
	Firstname *string
}

//
func (e Employee) SetName(name string) {
	if e.Firstname == nil {
		e.Firstname = &name
		return
	}
	*e.Firstname = name
}
