package main

import "fmt"

type Client struct {
	name string
}

type Person struct {
	name   string
	client *Client
}

// Pointer changes name on all intances
func setNameWithPointer(person *Person) {
	person.name = "xxx"
	person.client.name = "yyy"

	dereferenced := *person
	dereferenced.name = "NameDereferenced"

	fmt.Print("dereferenced: ", dereferenced, " person.name ", person.name)
}

// Param passed by value will not change name
func setName(person Person) {
	person.name = "arman"
	fmt.Print("name is: ", person.name)
}

func main() {

	// The goal is to test whether person.name changes
	person := Person{
		name: "elma",
		client: &Client{
			name: "amel",
		},
	}

	// Pointer will change name in `person` variable
	setNameWithPointer(&person) // points to memory address

	// Pass by value will not change `person` variable
	// setName(person)
	fmt.Print("\n")
	fmt.Println(person.name, person.client.name)
}
