package main

import "fmt"

func main() {
	h := House{
		name: "Green House",
	}
	p := Person{
		age:   10,
		house: &h,
	}

	fmt.Println(p.house)
	h.name = "Red House"
	fmt.Println(p.house)
}

type House struct {
	name string
}

type Person struct {
	age int
	// pointer will ensure that any changes to House will reflect everywhere
	// if pointer is not used, than changes would not be propagated
	house *House
}

func (p *Person) House() House {
	return *p.house
}
