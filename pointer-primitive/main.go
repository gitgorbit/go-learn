package main

import (
	"fmt"
	"strconv"
)

type Client struct {
	name *string
	code *int64
}

type Person struct {
	name   string
	client *Client
}

func main() {

	// The goal is to test whether person.name changes
	var name string = "arman"

	person := Person{name: "elma", client: &Client{name: &name}}

	fmt.Print("\n")
	fmt.Println(person.name, *person.client.name)

	var n *string
	var tmp string = "amel"

	n = &tmp
	fmt.Println(*n)

	// code := ""
	// var i int64 = 100
	// var codePtr *int64 = &i
	// if codePtr != nil {
	// 	code = strconv.FormatInt(*codePtr, 10)
	// }

	// fmt.Println(code)

	v := "100"
	var codePtr *int64
	if code, _ := strconv.ParseInt(v, 10, 64); code > 0 {
		codePtr = &code
	}
	c2 := &Client{
		code: codePtr,
	}
	fmt.Println(*c2.code)
}
