package main

import "fmt"

// https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/pointers-and-errors
func main() {
	w := Wallet{}
	w.Deposit(10)
	b := w.Balance()
	fmt.Println(b)
}

type Wallet struct {
	balance int
}

// In Go, when you call a function or a method the arguments are copied.
// Pointer not used on purpose to demostrate the issue with copying the arguments
func (w Wallet) Deposit(d int) {
	fmt.Println("wallet memory address: ", &w.balance)
	w.balance += d
}

func (w Wallet) Balance() int {
	fmt.Println("balance memory address: ", &w.balance)
	return w.balance
}
