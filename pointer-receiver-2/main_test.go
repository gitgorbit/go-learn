package main

import (
	"fmt"
	"testing"
)

func TestWallet(t *testing.T) {
	w := Wallet{}
	w.Deposit(10)

	got := w.Balance()

	fmt.Printf("address of balance in test is %v \n", &w.balance)

	want := 10
	if got != want {
		t.Errorf("got %d want %d", got, want)
	}
}
