package main

import "fmt"

type Actions interface {
	SetNameSurname(name, surname string) *Person
}

type Person struct {
	name, surname string
}

func (person *Person) SetNameSurname(name, surname string) *Person {
	person.name = name
	person.surname = surname
	return person
}

func main() {
	person := Person{name: "amel", surname: "husic"}

	renamed := person.SetNameSurname("elma", "catovic")
	fmt.Println(person.name, person.surname, renamed)
}
