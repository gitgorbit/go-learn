package main

import "fmt"

type SpoeTransaction struct {

	// version
	Version int64 `json:"_version,omitempty"`

	// id
	// Pattern: ^[^\s]+$
	ID string `json:"id,omitempty"`

	// status
	// Enum: [failed in_progress success]
	Status string `json:"status,omitempty"`
}

type SpoeTransactions []*SpoeTransaction

func main() {
	ts := SpoeTransactions{}
	ts = append(ts,
		&SpoeTransaction{
			Version: 1,
			ID:      "2",
			Status:  "failed",
		})

	if len(ts) > 0 {
		fmt.Println(ts[0].Status)
	}
}
