package main

import (
	"fmt"
	"os"
	"runtime/trace"
	"testing"
)

// https://medium.com/a-journey-with-go/go-should-i-use-a-pointer-instead-of-a-copy-of-my-struct-44b43b104963
// after running these benchmark tests run `go tool trace` to vizualize trace:
// go tool trace .\heap.out
// go tool trace .\stack.out
//
func BenchmarkMemoryStack(b *testing.B) {
	var s S
	f, err := os.Create("stack.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	for i := 0; i < b.N; i++ {
		s = byCopy()
	}
	trace.Stop()

	b.StopTimer()
	_ = fmt.Sprintf("%v", s.a)
}

func BenchmarkMemoryHeap(b *testing.B) {
	var s *S

	f, err := os.Create("heap.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}

	for i := 0; i < b.N; i++ {
		s = byPointer()
	}

	trace.Stop()

	b.StopTimer()

	_ = fmt.Sprintf("%v", s.a)
}
