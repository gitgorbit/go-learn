package main

import "fmt"

// https://medium.com/a-journey-with-go/go-should-i-use-a-pointer-instead-of-a-copy-of-my-struct-44b43b104963
type State struct {
	done bool
}

type Person struct {
	name, surname string
	state         State
}

func (person *Person) DoneTrue() {
	person.state = State{done: true}
}

func (person *Person) DoneFalse() {
	person.state.done = false
}

func main() {
	person := Person{
		name:    "amel",
		surname: "husic",
		state:   State{done: false},
	}

	// fmt.Println(person.name, person.surname)
	fmt.Println("done prev:", person.state.done)
	person.DoneTrue()
	fmt.Println("done after:", person.state.done)
}
