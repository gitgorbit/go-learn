package main

import (
	"context"
	"log"
	"runtime"
	"time"
)

func main() {
	log.Println("begin program")
	go launch()
	time.Sleep(time.Millisecond)
	// The number of goroutines
	log.Printf("Gouroutine count: %d\n", runtime.NumGoroutine())
	// for {
	// }
}

func doSth2(ctx context.Context) {
	// select {
	// case <-ctx.Done():
	// 	log.Println("second goroutine return")
	// 	return
	// }
	<-ctx.Done()
	log.Println("second goroutine return")
	return
}

func doSth(ctx context.Context) {
	// select {
	// case <-ctx.Done():
	// 	log.Println("first goroutine return")
	// 	return
	// }
	<-ctx.Done()
	log.Println("first goroutine return")
	return
}

func launch() {
	ctx := context.Background()
	// if cancel not provided then goroutine leaks occurs
	// ctx, _ = context.WithCancel(ctx)
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	log.Println("launch first goroutine")
	go doSth(ctx)
	log.Println("launch second goroutine")
	go doSth2(ctx)
}
