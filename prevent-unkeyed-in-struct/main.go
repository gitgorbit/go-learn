package main

import "fmt"

type Person struct {
	_       struct{}
	name    string
	surname string
}

func main() {
	p := Person{name: "amel", surname: "surname"}
	// p := Person{"amel", "surname"} // not OK
	fmt.Println(p)
}
