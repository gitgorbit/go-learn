package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"github.com/GehirnInc/crypt"
)

func main() {
	token := getToken()
	fmt.Println("Here is a random token : ", token)
}

func getToken() string {
	randBytes := make([]byte, 32)
	_, err := rand.Read(randBytes)
	if err != nil {
		panic(err)
	}
	// return base32.StdEncoding.EncodeToString(randBytes)[:50]
	return base64.URLEncoding.EncodeToString(randBytes)
}

//2r2VnTwAoxR4ftSzVKpLAL2kCdONOuMjZIXsRkdxiCQ=
//TMbJ5TwtD6qBHjparLizPXO0k-YJCf3KYZSxSvQ5dFM=
//dawBY028ONNw5nNNButiWnfONytheh6EZ8rVAPNaKrY=
//m2M8h68DEdvzgDbU3DlOKEFC_tBvpYJOqlGAUaIe0pQ=
//H5ODV4YHFMQ423DXUFGVNIUVVHE5
//3Go3dJHfzr9nJj2CpKMNFZb370xx017a8t1szs7lmyU=

func hashPassword(password string) (string, error) {
	crypter := crypt.SHA256.New()
	return crypter.Generate([]byte(password), []byte("$5$salt"))
}
