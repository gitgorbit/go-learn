package main

import (
	"fmt"
	"math/rand"
	"os"
	"runtime/trace"
	"strconv"
	"time"
)

//go tool trace -http=127.0.0.1:8003 trace.out
func traceFn() {
	//https://making.pusher.com/go-tool-trace/
	f, err := os.Create("trace.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()
}

func main() {
	traceFn()

	// fmt.Print(rand.Intn(100), ",")
	// fmt.Print(rand.Intn(100))
	// fmt.Println()

	// fmt.Println(rand.Float64())

	// fmt.Print((rand.Float64()*5)+5, ",")
	// fmt.Print((rand.Float64() * 5) + 5)
	// fmt.Println()

	// // s1 := rand.NewSource(time.Now().UnixNano())
	// // r1 := rand.New(s1)

	// // fmt.Print(r1.Intn(100), ",")
	// // fmt.Println()

	// s2 := rand.NewSource(42)
	// r2 := rand.New(s2)
	// fmt.Print(r2.Intn(100), ",")
	// fmt.Print(r2.Intn(100))
	// fmt.Println()
	// s3 := rand.NewSource(42)
	// r3 := rand.New(s3)
	// fmt.Print(r3.Intn(100), ",")
	// fmt.Print(r3.Intn(100))

	rand.Seed(time.Now().UTC().UnixNano())
	// max := 20
	// min := 10
	// fmt.Print(rand.Intn(max-min) + min)

	// fmt.Print(rand.Intn(2))

	r := strconv.FormatInt(100, 10)
	fmt.Print(r)
}
