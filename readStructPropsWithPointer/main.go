package main

import "fmt"

type Person struct {
	name, surname string
}

func (p *Person) String() (string, string) {
	return p.name, p.surname
}

func main() {
	person := Person{name: "amel", surname: "husic"}
	fmt.Println(person.name)

	// name, surname := person.String()

	copy := person

	fmt.Println(copy.name, copy.surname)
}
