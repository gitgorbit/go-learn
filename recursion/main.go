package main

import "fmt"

func main() {
	for v := range words(2) {
		fmt.Printf("%s\n", v)
	}
}

const (
	From = 97
	To   = 99
)

func words(length int) chan string {
	result := make(chan string, 100)
	go func() {
		words1(From, To, length, "", result)
		close(result)
	}()
	return result
}

func words1(from, to, length int, prefix string, result chan string) {
	if length > 2 && to-from > 1 {
		done := make(chan struct{})
		go func() {
			words1(from, (from+to)/2, length, prefix, result)
			close(done)
		}()
		words1((from+to)/2, to, length, prefix, result)
		<-done
		//return
	}
	for i := from; i < to; i++ {
		letter := fmt.Sprintf("%s%c", prefix, i)
		result <- letter
		if length > 1 {
			words1(From, To, length-1, letter, result)
		}
	}
}
