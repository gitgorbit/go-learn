package main

import (
	"fmt"
	"sync"
)

func recur(depth int, g *sync.WaitGroup) {

	defer g.Done()

	if depth <= 0 {
		return
	}

	fmt.Println(depth)

	for i := 0; i < 3; i++ {
		g.Add(1)
		go recur(depth-1, g)
	}
}

func main() {
	g := &sync.WaitGroup{}
	g.Add(1)
	recur(4, g)
	g.Wait()
}
