package main

import (
    "fmt"
    "reflect"
)

type MyStruct struct {
    Field1 int
    Field2 string
    Field3 float64
}

func main() {
    // Create a map with string keys and interface{} values
    myMap := map[string]interface{}{
        "Field1": 10,
        "Field2": "hello",
        "Field3": 3.14,
    }

    // Create an instance of MyStruct
    myStructInstance := MyStruct{}

    // Iterate over the map and use reflection to set struct fields
    structValue := reflect.ValueOf(&myStructInstance).Elem()
    for key, value := range myMap {
        structFieldValue := structValue.FieldByName(key)
        if !structFieldValue.IsValid() {
            fmt.Printf("Field %s does not exist in MyStruct\n", key)
            continue
        }

        if !structFieldValue.CanSet() {
            fmt.Printf("Cannot set value for field %s\n", key)
            continue
        }

        fieldValue := reflect.ValueOf(value)
        if structFieldValue.Type().AssignableTo(fieldValue.Type()) {
            structFieldValue.Set(fieldValue)
        } else {
            fmt.Printf("Value type mismatch for field %s\n", key)
        }
    }

    // Print the struct instance
    fmt.Println(myStructInstance)
}
