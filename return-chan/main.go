package main

import "fmt"

func main() {
	result := <-returnChan(5)
	fmt.Println(result)
}

func returnChan(val int) (result chan int) {
	result = make(chan int)
	go func() {
		defer close(result)
		result <- val
	}()
	return result
}
