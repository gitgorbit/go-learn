package main

import (
	"fmt"
	"path"
	"regexp"
)

func main() {
	r := SanitizeFilename("#4*")

	fmt.Println(r)
}

func SanitizeFilename(name string) string {
	reg := regexp.MustCompile(`[^a-zA-Z0-9#_-]+`)
	name = path.Clean(name)
	name = reg.ReplaceAllString(name, "_")
	return name
}
