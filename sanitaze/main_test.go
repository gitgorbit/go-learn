package main

import "testing"

func TestSanitizeFilename(t *testing.T) {
	tests := []struct {
		name    string
		input   string
		want    string
		wantErr bool
	}{
		{
			name:    "Should preserve # in input",
			input:   "#4_abc#",
			want:    "#4_abc#",
			wantErr: false,
		},
		{
			name:    "Should sanitaze input correctly",
			input:   "#1_?abc!&?",
			want:    "#1__abc_",
			wantErr: false,
		},
		{
			name:    "Should return same input when name doesn't contain regex characters",
			input:   "abcDEF",
			want:    "abcDEF",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SanitizeFilename(tt.input)
			if got != tt.want {
				t.Errorf("SanitizeFilename() = %v, want %v", got, tt.want)
			}
		})
	}
}
