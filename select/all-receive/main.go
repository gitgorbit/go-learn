package main

import (
	"fmt"
	"sync"
)

func main() {
	// program will randomly read from one of the chanels
	// WaitGroup ensures that main goroutine is not completed before at least one value is read from channel
	var wg sync.WaitGroup
	wg.Add(1)
	ch1 := make(chan string)
	ch2 := make(chan string)

	go func(ch chan string) {
		defer wg.Done()
		ch <- "one"
	}(ch1)
	go func(ch chan string) {
		defer wg.Done()
		ch <- "two"
	}(ch2)

	select {
	case v := <-ch1:
		fmt.Println(v)
	case v := <-ch2:
		fmt.Println(v)
	}
	wg.Wait()
}
