package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	ch1 := make(chan string)
	ch2 := make(chan string)

	go func(ch chan string) {
		defer wg.Done()
		fmt.Println(<-ch)
	}(ch1)

	go func(ch chan string) {
		defer wg.Done()
		fmt.Println(<-ch)
	}(ch2)

	select {
	case ch1 <- "one":
	case ch2 <- "two":
	}
	wg.Wait()
}
