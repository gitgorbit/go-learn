package main

import (
	"fmt"
	"sync"
)

func main() {

	// WaitGroup will ensure that at least one chanel is read before main goroutine is finished
	var wg sync.WaitGroup
	wg.Add(1)

	ch1 := make(chan string)
	ch2 := make(chan string)

	go func(ch chan<- string) {
		defer wg.Done()
		ch <- "send one"
	}(ch1)

	go func(ch chan string) {
		defer wg.Done()
		fmt.Println(<-ch)
	}(ch2)

	select {
	case v := <-ch1:
		fmt.Println(v)
	case ch2 <- "send two":
	}
	wg.Wait()
}
