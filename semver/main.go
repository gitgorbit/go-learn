package main

import (
	"fmt"

	"github.com/hashicorp/go-version"
)

func main() {
	v1, err := version.NewVersion("0.1.10")
	if err != nil {
		fmt.Println("ERROR")
	}
	v2, err := version.NewVersion("0.1.2")
	if err != nil {
		fmt.Println("ERROR")
	}
	if v1.LessThan(v2) {
		fmt.Printf("%s is less than %s", v1, v2)
	}

	if v1.GreaterThan(v2) {
		fmt.Printf("%s is greater than %s", v1, v2)
	}

}
