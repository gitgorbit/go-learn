https://www.geeksforgeeks.org/how-to-pass-a-slice-to-function-in-golang/


Definition
<h5> Slices are `reference types`. Normal behaviour is that if copy of slice is changed, then original slice would change too
</h5>

When passing a slice to function as a parameter:
* Original slice is modified when element(s) are changed directly (e.g. for loop)
    - by definition
* Original slice is not modified when elements are using append inside the function