package main

import "fmt"

func main() {
	arr := []string{"amel", "elma"}
	fmt.Println(arr)
	modifyWithAppend(arr)
	fmt.Println("after modify", arr)
}

func modifyWithAppend(arr []string) {
	// Here we only modify the slice
	// Using append function
	// Here, this function only modifies
	// the copy of the slice present in
	// the function not the original slice
	arr = append(arr, "new")
	fmt.Println("inside modify", arr)
}
