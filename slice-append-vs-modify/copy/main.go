package main

import "fmt"

// https://blog.golang.org/slices-intro
func main() {

	original := []string{"amel", "elma"}
	fmt.Println("before copy:", original)
	cop := []string{"copied"}
	// To increase the capacity of a slice one must create a new,
	// larger slice and copy the contents of the original slice into it.
	copy(original, cop)
	// Since cap is not increased, it will not grow
	fmt.Println("after copy will not grow since cap is not increased:", original) // [ copies, elma ]

	// ------------- THIS WILL NOT GROW THE SLICE
	s := []int{0, 1, 2}
	n := copy(s, s[1:]) // n == 2, s == []int{1, 2, 2}
	fmt.Printf("copied elements %v number of copied elements %d \n", s, n)

	// ---------- THIS EXAMPLE WILL GROW THE SIZE, BECAUSE ALOCATION WHICH IS DOUBLE SIZE OF SOURCE CAPACITY
	original2 := []string{"amel", "elma"}
	// need to alocate a double of source capacity
	dst := make([]string, len(original2), (cap(original2) + 1))
	copy(dst, original2)
	fmt.Println("after alocation copy:", original2)

}

// https://stackoverflow.com/questions/46128016/insert-a-value-in-a-slice-at-a-given-index
// insert will insert a new element at the specified index
func insert(arr []int, index int, element int) ([]int, error) {
	// need to add a new element to grow the cap of array.
	// This value would be overwritten
	// 1,2,3
	if index > len(arr) {
		return nil, fmt.Errorf("index out of range %v", index)
	}
	arr = append(arr, 0) // 1,2,3,0
	fmt.Println(arr[index+1:], arr[index:], arr)
	copy(arr[index+1:], arr[index:]) // index = 1 => [3,0] [2,3,0]
	fmt.Println(arr)
	arr[index] = element
	return arr, nil
}

func delete(arr []int, index int) ([]int, error) {
	if index > len(arr) {
		return nil, fmt.Errorf("index out of range %v", index)
	}
	// [1,2,3]
	copy(arr[index:], arr[index+1:])
	return arr[:len(arr)-1], nil
}
