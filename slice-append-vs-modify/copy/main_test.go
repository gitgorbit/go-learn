package main

import (
	"reflect"
	"testing"
)

func Test_insert(t *testing.T) {
	type args struct {
		arr     []int
		index   int
		element int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "should insert element at 2 index",
			args: args{
				arr:     []int{1, 2, 3, 4},
				index:   2,
				element: 10,
			},
			want: []int{1, 2, 10, 3, 4},
		},
		{
			name: "should insert element at 0 index",
			args: args{
				arr:     []int{1, 2, 3, 4},
				index:   0,
				element: 10,
			},
			want: []int{10, 1, 2, 3, 4},
		},
		{
			name: "should insert element at 4 index(last in this case)",
			args: args{
				arr:     []int{1, 2, 3, 4},
				index:   4,
				element: 10,
			},
			want: []int{1, 2, 3, 4, 10},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := insert(tt.args.arr, tt.args.index, tt.args.element); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("insert() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_delete(t *testing.T) {
	type args struct {
		arr   []int
		index int
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{
			name: "Delete at index 1",
			args: args{
				arr:   []int{1, 2, 3, 4, 5},
				index: 1,
			},
			want:    []int{1, 3, 4, 5},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := delete(tt.args.arr, tt.args.index)
			if (err != nil) != tt.wantErr {
				t.Errorf("delete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("delete() = %v, want %v", got, tt.want)
			}
		})
	}
}
