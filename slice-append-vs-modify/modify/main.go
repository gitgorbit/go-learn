package main

import "fmt"

func main() {
	arr := []string{"amel", "elma"}
	fmt.Println(arr)
	modify(arr)
	fmt.Println("after modify", arr)
}

func modify(arr []string) {
	// arr[0] = "modified"
	arr = append(arr, "modified")
}
