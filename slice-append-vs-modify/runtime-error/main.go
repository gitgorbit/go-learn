package main

import (
	"fmt"
	"time"
)

func main() {
	// to recover from any panic, use recover in defer function
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Catch exception:", err)
		}
	}()
	slice := []int{1, 2, 3}
	newSlice := slice[1:2]
	newSlice[2] = 10 // runtime error: index out of range
	// since error occured, program flow will be stoped
	counter := 1
	for {
		counter++
		time.Sleep(time.Second * 1)
		fmt.Println("counter", counter)
		if counter == 5 {
			return
		}
	}

}
