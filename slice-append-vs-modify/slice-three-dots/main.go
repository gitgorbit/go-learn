package main

import "fmt"

func main() {
	//both are array
	//slice := [3]int{1, 2, 3}
	//compiler will calculate len:
	slice := [...]int{1, 2, 3}
	newSlice := slice[0:1]
	newSlice[0] = 10 //slice changes will modify array, because slice is reference type
	fmt.Println(slice, newSlice)
}
