package main

import "fmt"

//https://tip.golang.org/doc/go1.2#three_index
func main() {
	slice := []int{1, 2, 3}
	// panic: runtime error: slice bounds out of range [::4] with capacity 3
	// newSlice := slice[0:1:4]

	// The main point of specifying capacity is if for example capacity < original slice capacity,
	// then access to original slice elements is protected (elements which are not sliced)
	newSlice := slice[0:1:2] // start : end : capacity
	fmt.Println(newSlice, cap(newSlice))

	newSlice2 := slice[0:1]
	fmt.Println(newSlice2, cap(newSlice2))
}
