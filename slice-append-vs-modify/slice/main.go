package main

import "fmt"

func main() {
	slice := []int{1, 2, 3}
	newSlice := slice[1:2]
	// changes to sliced slice will propagate to the original slice (slices are reference type)
	newSlice[0] = 10
	fmt.Println(slice, newSlice)
}
