package main

import (
	"fmt"
	"strconv"
)

func main() {
	arr := []string{"amel", "elma"}
	result := changeWithAppend(arr)
	for _, v := range result {
		println(v)
	}
	fmt.Println("--------------------")
	for i := 0; i < len(arr); i++ {
		println(arr[i])
	}

	fmt.Println("--------------------")

	arr2 := []string{"amel", "elma"}
	changeWithAssignment(arr2)
	result2 := changeWithAppend(arr2)
	for _, v := range result2 {
		println(v)
	}
	fmt.Println("--------------------")
	for i := 0; i < len(arr2); i++ {
		println(arr2[i])
	}
	fmt.Println("--------------------")
}

func changeWithAppend(arr []string) []string {
	arr = append(arr, "new")
	return arr
}

func changeWithAssignment(arr []string) []string {
	for i := 0; i < len(arr); i++ {
		arr[i] = strconv.Itoa(i) + arr[i]
	}
	return arr
}
