package main

import "fmt"

func main() {
	arr := []string{"one"}
	fmt.Println(cap(arr), len(arr), arr[1:])
}
