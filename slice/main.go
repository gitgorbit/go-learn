package main

import "fmt"

// https://go.dev/blog/slices-intro
func main() {
	g := []int{2, 3, 4}
	fmt.Println("elements", g, "len:", len(g), "cap", cap(g))
	slice := g[2:]
	slice[0] = 15

	// another way to increase a slice
	g = append(g, 1, 2, 3, 4)

	fmt.Println("elements", g, "len:", len(g), "cap", cap(g))
	fmt.Println(`
	After change, original slice will change too.
	Reason is that sliced slice is not a copy, but a pointer to original slice`,
		g)

	// -----------------------------------------
	// Third element will be 0, element len < array len
	e := [3]int{1, 2}
	fmt.Println(e)

	// -----------------------------------------
	// f := [3]int{1, 2, 3}
	// can not append to array
	//f = append(f, 3)

	// use array for fixed number of data, 16 bytes
	b := [16]string{"e39bdaf4-710d-42ea-a29b-58c368b0c53c"}
	fmt.Println(b)

	// create slice from array
	i := [3]int{1, 2, 3}
	iSlice := i[:]
	fmt.Println("iSlice", iSlice)
}
