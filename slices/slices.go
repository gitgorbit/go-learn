package main

import "fmt"

// passing pointer will ensure that any changes of array inside a func is reflected to parameter slice
// if * pointer is not used then original slice would not be changed
func simpleSlice(slice *[]int) {
	*slice = append(*slice, 0)
}

func main() {
	slice := []int{1, 2, 3, 4, 5}
	newSlice := append(slice, 2)
	fmt.Println(slice, newSlice)

	arr := []int{1, 2, 3}
	fmt.Println("before", arr)
	simpleSlice(&arr)
	fmt.Println("after", arr)
}
