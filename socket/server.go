package main

import (
	"fmt"
	"net"

	"github.com/prometheus/common/log"
)

func main() {
	// listener, err := net.Listen("tcp4", ":8080")
	// open in terminal and:
	// nc -U /tmp/amel.sock
	// type anything

	listener, err := net.Listen("unix", "/tmp/amel.sock")
	if err != nil {
		log.Fatal("error:", err)
	}

	conn, err := listener.Accept()
	for {
		buf := make([]byte, 512)
		nr, err := conn.Read(buf)
		if err != nil {
			return
		}
		data := buf[0:nr]
		fmt.Println(string(data))
	}

}
