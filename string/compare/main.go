package main

import (
	"fmt"
	"strings"
)

func main() {
	r1 := strings.Compare("3adada", "2gadad")
	fmt.Println(r1)

	// for i := 0; i < 10; i++ {
	// 	fmt.Println("w" > strconv.Itoa(i))
	// }
	// fmt.Println("end---------------")

	// r, err := compareVersion("0.2", "0.2.0")
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(r)

}

func compareVersion(v1, v2 string) (int, error) {
	a := strings.Split(v1, ".")
	b := strings.Split(v2, ".")

	if len(a) < 2 {
		return 0, fmt.Errorf("%s argument is not a version string", v1)
	}
	if len(b) < 2 {
		return 0, fmt.Errorf("%s argument is not a version string", v2)
	}

	if len(a) != len(b) {
		switch {
		case len(a) > len(b):
			for i := len(b); len(b) < len(a); i++ {
				b = append(b, " ")
			}
			break
		case len(a) < len(b):
			for i := len(a); len(a) < len(b); i++ {
				a = append(a, " ")
			}
			break
		}
	}

	var res int

	for i, s := range a {
		var ai, bi int
		fmt.Sscanf(s, "%d", &ai)
		fmt.Sscanf(b[i], "%d", &bi)

		if ai > bi {
			res = 1
			break
		}
		if ai < bi {
			res = -1
			break
		}
	}
	return res, nil
}
