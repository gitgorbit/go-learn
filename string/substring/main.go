package main

import (
	"fmt"
	"strconv"
)

func main() {
	res := []string{}
	payload := ""
	for i := 0; i < 10; i++ {
		payload += "k-" + strconv.Itoa(i) + " " + "v-" + strconv.Itoa(i) + "\n"
	}

	res = append(res, payload)
	fmt.Println(res)
}
