package main

import (
	"fmt"
	"strings"
)

func main() {
	s := "name  amel is  my"
	parts := strings.Fields(s)
	for _, p := range parts {
		fmt.Println(p)
	}

	// split := strings.Split(s, " ")
	// for _, s := range split {
	// 	fmt.Println(s)
	// }

	// arr := []string{"1", "2", "4"}

	// fmt.Println(len(arr))
}
