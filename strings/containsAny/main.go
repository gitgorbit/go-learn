package main

import (
	"fmt"
	"strings"
)

func main() {

	parts := strings.Split("\"SameSite=Lax\r\"", " ")
	for i := 0; i < len(parts); i++ {
		fmt.Println(parts[i])
	}

	result := strings.ContainsAny("\"SameSite=Lax\t\"", "\x00\a\b\t\n\v\f\r;")
	fmt.Println(result)

	fmt.Println(rune('\x00'))

	run := '\a'
	fmt.Println(run)

	run = '\b'
	fmt.Println(run)

	run = '\t'
	fmt.Println(run)

	run = '\n'
	fmt.Println(run)

	run = '\v'
	fmt.Println(run)

	run = '\f'
	fmt.Println(run)

	run = '\r'
	fmt.Println(run)
}
