package main

import "fmt"

func main() {

	// The goal is to test whether person.name changes
	var name string = "arman"

	name = fmt.Sprintf("%s %s ", name, "elma")
	name = fmt.Sprintf("%s %s", name, "melisa")
	name = fmt.Sprintf("%s", name)

	fmt.Println(name)

}
