// +build ignore

package main

import "fmt"

type Shape struct {
	x float64
	y float64
}

func main() {
	var shape Shape
	xAxis := &shape.x

	*xAxis = 12.2
	fmt.Println(xAxis, shape.x)
}
