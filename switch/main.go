package main

import "fmt"

func main() {
	i := 12
	switch {
	case i == 12:
		fmt.Println("i = 12", i)
	case i > 10:
		fmt.Println("i>10", i)
	default:
		fmt.Println("default", i)
	}
}
