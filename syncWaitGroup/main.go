package main

import (
	"fmt"
	"sync"
)

func main() {

	wg := &sync.WaitGroup{}
	var counter int

	wg.Add(2)
	add := func() {
		counter++
		wg.Done()
	}

	go add()
	go add()
	wg.Wait()

	fmt.Println(counter)
}
