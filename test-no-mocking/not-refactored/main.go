package main

import (
	"bufio"
	"fmt"
	"os"
)

var FileName string = "/tmp/test.txt"

func main() {
	f, err := os.Create(FileName)
	if err != nil {
		fmt.Print(err.Error())
	}
	_, err = f.WriteString("HELLO")
	if err != nil {
		fmt.Print(err.Error())
	}
	err = analyze(FileName)
	if err != nil {
		fmt.Print(err.Error())
	}
}

func analyze(file string) error {
	handle, err := os.Open(file)

	if err != nil {
		return err
	}
	defer handle.Close()

	scanner := bufio.NewScanner(handle)
	for scanner.Scan() {
		// Do something with line
		_ = scanner.Text()
	}
	return nil
}
