// https://yourbasic.org/golang/three-dots-ellipsis/

package main

import "fmt"

// Variadic parameter. Three dots means any number of arguments can be passed to function e.g. Sum(1,3,54) or Sum(2,4)
func Sum(nums ...int) int {
	result := 0
	for _, n := range nums {
		result += n
	}
	return result
}
func main() {
	numbers := []int{1, 3, 5, 6}
	result := Sum(numbers...)

	fmt.Println(result)
}
