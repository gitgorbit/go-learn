package main

import (
	"log"
	"math/rand"
	"time"
)

func main() {
	interval := 5
	ticker := time.NewTicker(time.Second * time.Duration(interval))

	for range ticker.C {
		b := New()
		b.BuyOrSell()

		switch b.TradingState() {
		case Buy:
			interval = 3
			ticker.Reset(time.Duration(interval) * time.Second)
			log.Println("watcher is running at interval...", interval)
		case Sell:
			interval = 2
			ticker.Reset(time.Duration(interval) * time.Second)
			log.Println("watcher is running at interval...", interval)
		}

		log.Println("watcher is running at interval...", interval)
	}
}

type State string

const (
	Buy  State = "buy"
	Sell State = "sell"
)

type Buyer struct {
	state State
}

func New() *Buyer {
	return &Buyer{
		state: Buy,
	}
}

func (b *Buyer) BuyOrSell() {
	r := rand.Intn(10)
	if r%2 == 0 {
		b.state = Sell
	} else {
		b.state = Buy
	}
}

func (b *Buyer) TradingState() State {
	return b.state
}
