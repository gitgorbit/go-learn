package main

import (
	"fmt"
	"math/rand"
	"os"
	"runtime/trace"
	"time"
)

// Test data race:
// go run -race trace/race/main.go
// Inspect generated trace.out with:
// go tool trace -http=127.0.0.1:8003 trace.out

func traceFn() {
	//https://making.pusher.com/go-tool-trace/
	f, err := os.Create("trace.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()
}

func main() {
	traceFn()

	start := time.Now()
	var t *time.Timer
	t = time.AfterFunc(randomDuration(), func() {
		fmt.Println(time.Now().Sub(start))
		t.Reset(randomDuration())
	})
	time.Sleep(5 * time.Second)
}

func randomDuration() time.Duration {
	return time.Duration(rand.Int63n(1e9))
}
