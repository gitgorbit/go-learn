package main

import (
	"fmt"
	"time"
)

func main() {
	ping := make(chan struct{}, 1)
	pong := make(chan struct{}, 1)

	for {
		select {

		case ping <- struct{}{}:
		case pong <- struct{}{}:

		case <-ping:
			fmt.Println("ping")
			time.Sleep(time.Second * 2)
		case <-pong:
			fmt.Println("pong")
			time.Sleep(time.Second * 2)
		}
	}
}
