package main

import (
	"fmt"
	"sync"
)

type Human struct {
	name    string
	surname string
}

// Person is new type alias
type Person = Human

type SameMutex = sync.Mutex

type NewMutex sync.Mutex //nolint
// NewMutex is new type and does not inherit any methods from sync.Mutex

func main() {
	p := Person{name: "Amel", surname: "Husic"}
	fmt.Println(p.name, p.surname)

	s := SameMutex{}
	s.Lock()
	fmt.Println("locked")
	s.Unlock()
	// n := NewMutex{}
	// n.Lock() // doesn't exists
}
