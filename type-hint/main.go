package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	a := &A{}
	value := Decode(a, []byte(`{"X":"10", "Y":"20"}`)) // & pointer must be used for Unmarshal func
	fmt.Println(value, a)
}

type A struct {
	X string
	Y string
}

func Decode(useThisType interface{}, binaryData []byte) interface{} {
	json.Unmarshal(binaryData, useThisType)
	return useThisType
}
