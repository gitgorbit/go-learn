// +build ignore

package main

import "fmt"

func main() {
	a := 15
	b := &a
	fmt.Println(b)  // points to memory address 0xc00001a0d8
	fmt.Println(*b) // READ value through pointer

	*b = 5 // WRITE a value of 'a' through pointer
	fmt.Println(a)

	*b = *b * *b
	fmt.Println(a)
}
