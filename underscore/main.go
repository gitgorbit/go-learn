package main

import "fmt"

func main() {
	p := Person{10, "amel", "engineer"}
	fmt.Println(p)
}

type Person struct {
	_    int
	name string
	job  string
}
