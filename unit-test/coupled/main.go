package main

import (
	"bytes"
	"encoding/xml"
	"io"
	"io/ioutil"
	"os/exec"
	"strings"
)

//https://dev.to/quii/how-to-test-a-function-that-uses-osexec-4m5a
func main() {

}

type Payload struct {
	Message string `xml:"message"`
}

// func GetData() string {
// 	cmd := exec.Command("cat", "msg.xml")

// 	out, _ := cmd.StdoutPipe()
// 	var payload Payload
// 	decoder := xml.NewDecoder(out)

// 	// these 3 can return errors but I'm ignoring for brevity
// 	cmd.Start()
// 	decoder.Decode(&payload)
// 	cmd.Wait()

// 	return strings.ToUpper(payload.Message)
// }

func GetData(data io.Reader) string {
	var payload Payload
	xml.NewDecoder(data).Decode(&payload)
	return strings.ToUpper(payload.Message)
}

func getXMLFromCommand() io.Reader {
	cmd := exec.Command("cat", "msg.xml")
	out, _ := cmd.StdoutPipe()

	cmd.Start()
	data, _ := ioutil.ReadAll(out)
	cmd.Wait()

	return bytes.NewReader(data)
}
