package main

import (
	"strings"
	"testing"
)

// func TestGetData(t *testing.T) {
// 	got := GetData()
// 	want := "HAPPY NEW YEAR!"

// 	if got != want {
// 		t.Errorf("got '%s', want '%s'", got, want)
// 	}
// }

func TestGetData(t *testing.T) {
	input := strings.NewReader(`
<payload>
    <message>Cats are the best animal</message>
</payload>`)

	got := GetData(input)
	want := "CATS ARE THE BEST ANIMAL"

	if got != want {
		t.Errorf("got '%s', want '%s'", got, want)
	}
}
