// +build ignore

package main

import (
	"fmt"
	"net/url"
	"path"
)

func main() {
	u, _ := url.Parse("http://192.168.122.237:4444")

	u.Path = path.Join("api", "action.APIVersion", "scope/root/l7/config", "action.ConfigHash")

	fmt.Println(u.String())
}
