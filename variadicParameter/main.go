package main

import "fmt"

func main() {

	testVariadic(true)
	testVariadic(false)
	testVariadic()
}

func testVariadic(optional ...bool) {
	// if len(optional) > 0 {
	// 	fmt.Println(optional[0])
	// }
	dereference(optional...)
}

func dereference(optional ...bool) {
	if len(optional) > 0 {
		fmt.Println(optional[0])
	}
}
