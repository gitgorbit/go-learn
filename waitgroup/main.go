package main

import (
	"fmt"
	"sync"
)

func main() {
	execute()
}

func execute() error {
	wg := &sync.WaitGroup{}

	wg.Add(2)
	go fn1(wg)
	go fn2(wg)
	wg.Wait()

	return nil
}

func fn1(wg *sync.WaitGroup) {
	fmt.Println("fn1")
	wg.Done()
}

func fn2(wg *sync.WaitGroup) {
	fmt.Println("fn2")
	wg.Done()
}
