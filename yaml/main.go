package main

import (
	"fmt"
	"strings"

	"github.com/ghodss/yaml"
)

type tag struct {
	Name        string "json:\"name,omitempty\""
	Description string "json:\"description,omitempty\""
}

type tags []tag

func main() {
	var str string = `
- name: Discovery
  description: API autodiscover endpoints
- name: Information
- name: Specification
- name: Transactions
  description: |
   Managing transactions. Configuration changes can be grouped in the transaction. You start the
   transaction with trasactions POST, and call the configuration changes you need with parameter
   transaction_id. When you want to commit the transaction, you call the transactions PUT and all changes
   in that transaction is commited. If you call a configuration change without the transaction_id,
   transaction mechanism is implicitly called with one operation in transaction.
- name: Reloads
  description: Simple description
`

	var ts tags = tags{}

	err := yaml.Unmarshal([]byte(str), &ts)
	if err != nil {
		fmt.Println(err.Error())
	}

	for _, t := range ts {
		fmt.Println("name: ", t.Name)
		if t.Description != "" {
			fmt.Println(" descr: ", t.Description)
		}
	}

	var sb strings.Builder

	sb.WriteString("amel")
	sb.WriteString("elma")
	fmt.Println(sb.String())
}
